---
layout: home
title: Start
permalink: /
nav_order: 10
---

<div class="btn-pink" style="padding: 1em">
  <a href="https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf"><img src="/s4f-regionalplan.png" width="632" height="651" style="display: block; width: 40%; height: auto; aspect-ratio: attr(width) / attr(height); float: left; padding-right: 1em"></a>

  <strong>Der Regionalplan ist ein Klimahöllenplan!</strong><br><br>

  Es liegt nun ein <strong>wissenschaftliches Gutachten</strong> zum aktuellen
  Regionalplanentwurf vor, das sachlich fundiert eine ganze Reihe von
  Unstimmigkeiten und professionellem Fehlverhalten seitens des
  Regionalverbands dokumentiert.<br><br>

  👩‍🔬 <a style="color: white" href="https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf">Direkt zum Gutachten</a> (69 Seiten)<br>
  🌳 <a style="color: white" href="/regionalplan">Was ist überhaupt der Regionalplan?</a>
  <div style="clear:both"></div>
</div>

<style>
  .carousel-container { position: relative; }
  .carousel { overflow-x: scroll; scroll-snap-type: x mandatory; scroll-behaviour: smooth; display: flex; }
  .carousel a { flex-shrink: 0; scroll-snap-align: start; }
</style>

<div class="carousel-container" style="padding-top: 1em">
  <div class="carousel">
    <img src="/altdorfer-wald-besetzt.jpeg" width="640" height="480" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Der Altdorfer Wald ist besetzt.">
    <img src="/rvbo-1.jpeg" width="1280" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-2.jpeg" width="1280" height="960" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-3.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-4.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-5.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-6.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-7.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-8.jpeg" width="720" height="1280" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
    <img src="/rvbo-9.jpeg" width="1200" height="800" style="display: block; width: auto; height: 20em; aspect-ratio: attr(width) / attr(height)" alt="Kiesexport und Asphaltwahn – das ist ein Klima-Höllen-Plan! Für einen zukunftsfähigen Regionalplan!">
  </div>
</div>

# Baumhausklimacamp in Ravensburg

* **[Professor Ertel](https://www.hs-weingarten.de/~ertel/index.php)**
  war auf unserem Baumhaus!
* Am Abend des 29.12.2020 wurden wir
  **[geräumt](/pages/Pressemitteilungen/2020-12-29-Raeumung.html){: .text-red-300 :}**.
* Am Abend des 30.12.2020 errichteten wir ein **[neues Baumhaus](/pages/Pressemitteilungen/2020-12-30-Kraft.html)**.
  Ihr könnt unsere Häuser zerstören, aber nicht die Kraft, die sie schuf!
  #nichtmituns #fightfor1point5 #wircampenbisihrhandelt
* Vorerst gilt eine [Absichtserklärung](/absichtserklaerung-bis-2021-01-10.pdf) von uns und der Polizei.
* Am 30.1.2021 pausierten wir unser Baumhausklimacamp
  **[mit einem Ultimatum](/pages/Pressemitteilungen/2021-01-22-Pause.html)**.
* Die Scientists for Future veröffentlichten ihre **[Stellungnahme zum Regionalplanentwurf](https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf)**.
* Seit dem 25.2.2021 ist der Altdorfer Wald **[besetzt](/altdorfer-wald/)**.
{: .infolist :}

[Der Regionalplan ist ein Klimahöllenplan -- Details](/regionalplan/){: .btn .btn-pink }
[Wir schützen den Altdorfer Wald, indem wir ihn besetzen](/altdorfer-wald/){: .btn .btn-purple }

Der fünfte Jahrestag des Pariser Klimaabkommens ist verstrichen und wir
befinden uns noch immer weit davon entfernt Politik zu führen, die mit dem
1,5-Grad-Ziel übereinstimmt. 

Stattdessen werden Projekte finanziert, die schon lange nicht mehr zeitgemäß
sind. So wird im Moment in Hessen ein Jahrhunderte alter Mischwald gefällt und
wozu? Für eine Autobahn. Verkehrswende geht anders! Daher streiken wir. Im
[Danni](https://waldstattasphalt.blackblogs.org/) und jetzt auch hier! Kommt
gerne dazu, auch Bodensupport ist immer gerne gesehen! 💪

Wir fragen: Mit welchem Recht nimmt sich die Stadt Ravensburg
heraus, ihr [CO₂-Restbudget](/co2-budget/) um mehr als Doppelte zu überschreiten?
#nichtmituns #fightfor1point5 #dannibleibt #wircampenbisihrhandelt

**Update**: Am Abend von
Tag 18 (29.12.) wurden wir [geräumt](/pages/Pressemitteilungen/2020-12-29-Raeumung.html),
am nächsten Tag errichteten wir ein neues Baumhaus. An Tag 50 (30.1.)
pausierten wir unser Camp mit einem Ultimatum, um der trägen Stadtverwaltung
Zeit zu geben, ihre Versprechen einzuhalten. Bis zum 1. Mai wird es von uns
kein neues Baumhaus in Ravensburg geben. Wohl aber zahlreiche Einzelaktionen, insbesondere
zum aktuellen Regionalplanentwurf. [Hier Petition
unterschreiben!](https://fairwandel-sig.de/regionalplan/) Seit dem 25.2. ist
der [Altdorfer Wald besetzt](/altdorfer-wald/).

<img src="/icons/telegram.svg" style="width: 1em; height: 1em">
**[Infokanal auf Telegram](https://t.me/klimacamp_ravensburg)**
(nur wenig Nachrichten; in der verknüpften Diskussionsgruppe auch Meldungen
über aktuelle Entwicklungen und Möglichkeit zum direkten Kontakt)

<img src="/icons/instagram.svg" style="width: 1em; height: 1em">
**[Instagram @baumbesetzung.ravensburg](https://www.instagram.com/baumbesetzung.ravensburg/)**

[Pressemitteilungen](/pressemitteilungen/){: .btn .btn-green }
[Fotos](https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/){: .btn .btn-blue }
[Einladungsvideo](https://www.youtube.com/watch?v=8XOg_OQZueA){: .btn .btn-purple }
[Unsere Geschichte](/geschichte/){: .btn .btn-gray }
[Fragen und Antworten](/faq/){: .btn .btn-pink }


## Ort

Das Baumhausklimacamp befand an sich an der [Ecke
Karlstraße/Eisenbahnstraße](https://www.google.com/maps/place/47%C2%B046'59.7%22N+9%C2%B036'34.7%22E/@47.7833776,9.6093464,58m/data=!3m1!1e3!4m5!3m4!1s0x0:0x0!8m2!3d47.783237!4d9.609628)
in 88212 Ravensburg (vormals [Ecke
Schussenstraße/Grüner-Turm-Straße](https://www.openstreetmap.org/note/2461517)).
Momentan ist es pausiert.

Melde dich, wir helfen dir, ein
Baumhausklimacamp in deiner eigenen Stadt zu errichten!


## Impressum

Diese Webseite wird gepflegt von: Ingo Blechschmidt, Arberstr. 5, 86179
Augsburg, [+49 176 95110311](tel:+4917695110311), [iblech@web.de](mailto:iblech@web.de). Diese Person stellt auch gerne den
Kontakt zum Baumhaus her. Das Klimacamp wird von Aktivist\*innen diverser
Klimagerechtigkeitsbewegungen gemeinschaftlich organisiert. In Deutschland gibt
es [noch viele weitere Klimacamps](https://klimacamp.eu/) und Solibaumhäuser
für den [Danni](https://waldstattasphalt.blackblogs.org/).

<script data-no-instant>
  {% include instantclick.min.js %}
  InstantClick.init();
</script>
