---
layout: page
title: Rechtshilfe
permalink: /rechtshilfe/
nav_order: 90
---

# Rechtshilfe

**Update 1.1.2021.** Nach der Räumung des ersten Baumhauses und der Errichtung
des zweitens nahm die Polizei Verhandlungen mit uns auf. Wir einigten uns auf
einen Deal, der uns bis zum 10.1. freies Geleit aufs Baumhaus und weg vom
Baumhaus garantiert. Bis zum 10.1. ist es rechtlich also völlig unbedenklich,
Zeit auf dem Baumhaus zu verbringen. (Und auch danach ist es keine Straftat.
Siehe Rest dieser Seite.)

Leider kriminalisiert die Stadt Ravensburg unseren legitimen Protest. Dabei ist
er dringend notwendig, wenn sich mensch mal die [CO2-Bilanz](/co2-bilanz/) von
Ravensburg so ansieht.

Allen, die sich tiefer mit den rechtlichen Gegebenheiten auseinandersetzen
möchten, empfehlen wir folgende Dokumente:

1. [Die Zusammenfassung von *Am Boden bleiben*](https://www.ambodenbleiben.de/aktion-rechtliches/)
2. [Die Rechtshilfebroschüre zum Danni](https://waldstattasphalt.blackblogs.org/wp-content/uploads/sites/1055/2020/10/danni-ea-heftchen.pdf)
3. [Alle Broschüren der Projektwerkstatt](https://projektwerkstatt.de/index.php?domain_id=31&a=20114)

Es folgt nur eine Kurzzusammenfassung.


## Repression

In den letzten Tagen war die Polizei nur selten vor Ort und erkundigte sich dabei lediglich
nach dem aktuellen Stand. In der Anfangszeit warfen uns aber sowohl die Polizei
als auch der Oberbürgermeister unbegründet verschiedene Straftaten vor. Falls
es wieder dazu kommen sollte, lasst euch davon nicht stressen und bewahrt Ruhe!
Bei dominantem Auftreten ist es leider nicht immer leicht, sich zu erinnern,
wieso wir hier sind und welch großes Unrecht unsere Regierungen tun.
Schlussendlich steht auf der einen Seite klimapolitisches Totalversagen und auf
der anderen Seite eine kleine Gruppe Menschen auf einem Baum.

Bisher wurde keine\*r von uns rechtlich belangt. Falls es dazu kommt, steht uns eine
tolle solidarische Anwältin zur Verfügung, mit der wir schon seit langer Zeit
zusammenarbeiten. Sie vertrat etwa auch das [Augsburger
Klimacamp](https://augsburg.klimacamp.eu/).


## Rechtliche Einschätzung

Aus unserer Sicht ist unser Baumhausdorf von der Versammlungsfreiheit gedeckt.
Das gilt auch nachts für die Zeit der Ausgangsbeschränkung (§ 11 der
[Corona-Verordnung](https://www.baden-wuerttemberg.de/en/service/aktuelle-infos-zu-corona/aktuelle-corona-verordnung-des-landes-baden-wuerttemberg/)).
Zwar erklärte die Polizei unsere Versammlung bereits an Tag 1 für aufgelöst.
Nach unserer Ansicht war diese Auflösung jedoch rechtswidrig, da nicht die von
§ 13 des
[Versammlungsgesetz](https://www.landesrecht-bw.de/jportal/?quelle=jlink&query=VersammlG&psml=bsbawueprod.psml)
geforderte besondere Gefahr für die öffentliche Sicherheit bestand.

Auch falls Gerichte zu einer anderen Einschätzung gelangen sollten, stehen nur
drei Ordungswidrigkeiten (wie etwa auch Falschparken eine ist) im Raum:

1. Unerlaubtes Klettern auf einen Baum
2. Verstoß gegen Corona-Regeln
3. Teilnahme an aufgelöster Versammlung

Das sind keine Straftaten. Auch sich räumen zu lassen ist keine Straftat!

[Ausführliche rechtliche Einschätzung](/rechtliche-einschaetzung-baumhaus.pdf){: .btn .btn-purple }


## Personalien verweigern?

In Deutschland gibt es keine Pflicht, Ausweisdokumente mit sich zu führen.
Auf Nachfrage muss mensch aber der Polizei seine Personalien wahrheitsgemäß
angeben. Wie das Bundesverfassungsgericht bestätigte, gilt das jedoch nicht bei
der Teilnahme an Versammlungen (siehe etwa Absatz 11 von [1 BvR
1564/92](https://www.bundesverfassungsgericht.de/SharedDocs/Entscheidungen/DE/1995/03/rs19950307_1bvr156492.html)).

Bisher gaben manche von uns die Personalien an, während andere sie
verweigerten.


### Vorteile der Personalienverweigerung

* Erschwerung der behördlichen repressiven Verfolgung
* Solidarität mit Aktivisti, die schon Repression erlitten
* Aus Prinzip! Unser Protest ist legitim und geboten, die städtische
  Kriminalisierung ist verhältnislos und richtig eklig.


### Nachteile der Personalienverweigerung

* Bußgeld bis 1.000 €, falls die Personalien trotz Verweigerung festgestellt
  werden können, da Personalienverweigerung eine Ordnungswidrigkeit ist
* Möglicherweise stärkere mediale Wirkung bei Nichtverweigerung der Personalien
* Falls nicht im Baum, Gefahr auf Mitnahme zur Gefangenensammelstelle (Stunden
  oder ein Tag)


## Verhalten in der Gefangenensammelstelle

Es ist sehr unwahrscheinlich, dass einzelne von uns zur Gefangenensammelstelle
(Gesa) der Polizei gebracht werden. Allein schon, weil es nicht leicht ist, uns
aus acht Metern Höhe zu räumen. Falls es doch irgendwie passiert, beachtet:

1. Ruhig bleiben!
2. Nichts unterschreiben, auch nichts vermeintlich Harmloses wie Protokolle.
   (Übt diesen Satz: "Das möchte ich nicht unterschreiben.")
3. Konsequent Aussage verweigern. (Übt diesen Satz: "Dazu möchte ich nichts
   sagen." Ihr werdet ihn oft sagen müssen.)
4. Auf euren erfolgreichen Anruf bestehen und den Ermittlungsausschuss
   informieren (Ingo Blechschmidt, +49 176 95110311). Ihr seid nicht allein!
