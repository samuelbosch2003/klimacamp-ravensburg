---
layout: page
title: Interviews from the new tree house (2021-01-03)
permalink: /aktionen/interview/
nav_order: 3
parent: Aktionen
---

# Interviews from the new tree house (3.1.2021)

<figure>
  <video controls width="640" height="360" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/interview-2021-01-03-en.jpeg" data-setup="{}" class="video-js vjs-default-skin" style="width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/medium-interview-2021-01-03-en.mp4" type="video/mp4" label="360p" res="360">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-interview-2021-01-03-en.mp4" type="video/mp4" label="180p" res="180">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/interview-2021-01-03-en.mp4" type="video/mp4" label="720p" res="720">
  </video>
  <figcaption>in English</figcaption>
</figure>

<figure>
  <video controls width="640" height="360" preload="none" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/interview-2021-01-03-de.jpeg" data-setup="{}" class="video-js vjs-default-skin" style="width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/medium-interview-2021-01-03-de.mp4" type="video/mp4" label="360p" res="360">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/small-interview-2021-01-03-de.mp4" type="video/mp4" label="180p" res="180">
    <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/interview-2021-01-03-de.mp4" type="video/mp4" label="720p" res="720">
  </video>
  <figcaption>auf Deutsch</figcaption>
</figure>

