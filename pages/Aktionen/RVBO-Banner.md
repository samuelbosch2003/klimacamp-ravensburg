---
layout: page
title: Kundgebung mit Dachbanner beim RVBO
permalink: /aktionen/rvbo-banner/
nav_order: 5^
parent: Aktionen
---

# Kundgebung mit Dachbanner beim RVBO (6.2.2022)

<figure style="float: right">
  <img src="/kundgebung-2021-02-06.jpeg.jpeg" width="450" height="449" style="display: block; width: 100%; height: auto; aspect-ratio: attr(width) / attr(height); padding: 0.5em">
</figure>

**Regionalplan, was ist das?**
Er wird vom Regionalverband neu geschrieben, kaum ein Mensch weiß wer dieses Gremium ist. Unsere Umweltverbände und die Wissenschaft **Scientist for Future** warnen davor, warnen davor weil die Rechnung ohne den Klimawandel gemacht wurde. 
Kommt vorbei, hört zu, denn die Profis stellen am Samstag den **6.2.2021** einen **zukunftsfähigen Gegenentwurf** vor!!! **Coronakonforme Aktionen** und **Redebeiträge** dann um **11 und 14 Uhr** am **Regionalverband (Hirschgraben 2)** in Ravensburg.

Einladung zum Protest, gerne weiterleiten. Bitte denkt an **FFP2-Masken** und genügend **Abstand**.
 
Schreibt auch gerne in eine der verlinkten Mustereinwendungen noch was Persönliches/Fehlendes dazu, druckt es aus und bringt den Brief mit (oder schickt es per Mail an [info@rvbo.de](mailto:info@rvbo.de), wir wollen den Postkasten des RVBO füllen ;-) Hier gibt es weitere Mustereinwendungen: 
[allgemein](/mustereinwendung-1.docx)
[Klima](/mustereinwendung-2.docx)
[Gewerbe](/mustereinwendung-3.docx)
[Wohnraum](/mustereinwendung-4.docx)
[Ländle4Future](https://regionbodenseeoberschwaben.blogspot.com/search/label/Muster%20Einwendungen)
[altdorferwald.org](https://altdorferwald.org/133/informationen-veranstaltungen/2-offenlegung-regionalplan)
