---
layout: page
title: Pausierung und Ultimatum
permalink: /aktionen/pausierung/
nav_order: 4
parent: Aktionen
---

# Pausierung und Ultimatum (30.1.2021)

<figure style="float: right">
  <img src="/kundgebung-2021-01-30.jpeg" width="640" height="640" style="display: block; width: 100%; height: auto; aspect-ratio: attr(width) / attr(height); padding: 0.5em">
</figure>

**Wir bauen das Baumhaus am 30.1. vorerst ab!**
Es wird zudem eine Kundgebung mit Redebeiträgen geben.
**Kommt gerne vorbei! (Coronakonform mit Abstand und FFP2-Masken)**
  
Wir geben der Stadt Zeit, ihre Versprechungen und Ziele umzusetzen. Jetzt kann die Umsetzungslücke geschlossen werden.
Wenn die Stadt bis zum 1. Mai handelt, bauen wir kein neues Baumhaus.

Um die Stadt dennoch dauerhaft zu erinnern, werden wir an vier Standorten in
der Stadt Banner aufhängen. Außerdem sind weitere kreative Protestaktionen geplant.

**Dabei sind alle herzlich eingeladen mitzuwirken und eigene Ideen einzubringen!**
#actnow #climatejustice #whatdowewant #fightfor1point5

**[Soli-Gruppe auf Telegram](https://t.me/joinchat/MW8ulxlUZK9yJQFY-LiBcA)**
