---
layout: page
title: Spenden
permalink: /spenden/
nav_order: 130
---

# Spenden

Seit dem 14.1. können wir auch per Überweisung Spenden entgegennehmen. Wir selbst engagieren uns
natürlich ehrenamtlich; bisher hatten wir Ausgaben ist Höhe von 7.023,42 Euro
(Stand 7.4.2021; vor allem hunderte Meter Polyprop sowie Kletterausrüstung),
die einige von uns privat vorstreckten, und Spenden in Höhe von 4.378,31 Euro (Stand 7.4.2021).

In Zukunft werden vermutlich, je nach
Verhalten der Stadt, Anwaltskosten fällig. Uns wurde auch eine Räumungsgebühr
in Höhe von 4.053,50 Euro in Aussicht gestellt, wir sind aber zuversichtlich, dass
das Verwaltungsgericht diese kippen wird.

Diese Räumungsgebühr mussten wir trotz des anhängigen Verfahrens auslegen.
Diese Auslage übernahm eine Privatperson. Wir schätzen das Engagement dieser
Person zutiefst!

    Name: Spenden & Aktionen
    IBAN: DE29 5139 0000 0092 8818 06
    BIC:  VBMHDE5F
    Verwendungszweck: Ravensburg (unbedingt angeben, sonst kann die Spende nicht zugeordnet werden!)

Hinweis: Wir können zwar Spendenbescheinigungen ausstellen -- [schreibt
uns](mailto:baumbesetzung.ravensburg@gmail.com) dazu an -- der Treuhandverein
ist vom Staat aber nicht als gemeinnützig anerkannt.
