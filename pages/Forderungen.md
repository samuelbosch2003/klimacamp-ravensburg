---
layout: page
title: Forderungen
permalink: /forderungen/
nav_order: 70
---

# Forderungen

**Unsere Forderungen entstehen in einem offenen, hierarchiearmen Prozess. Alle
Interessierten sind herzlich eingeladen, an ihrer Gestaltung mitzuwirken. Die
hier bereits veröffentlichten Forderungen sind als ersten Entwurf und
vorläufige Momentaufnahme zu verstehen, bis mehr Menschen in die Konsensbildung
mit einbezogen werden konnten.**


## 1. CO₂-Budget zum 1,5-Grad-Limit einhalten

Damit Ravensburg seinen Anteil am Pariser Klimaabkommen einhalten kann, darf
die Stadt Ravensburg nur noch [1,8 Millionen Tonnen CO₂](/co2-budget/)
emittieren. Die Stadt muss verbindlich festschreiben, dieses Restbudget nicht
zu überreizen, sich sektorielle Jahreszwischenziele setzen, jährlich öffentlich
über den Fortschritt Bericht erstatten und Steuerungsinstrumente zur
Nachjustierung vorsehen. Momentan plant die Stadt, mehr als das Doppelte zu
verantworten. 


## 2. Angemessene Verkehrswende

### a) Autofreie Innenstadt

Autos nehmen uns in der Innenstadt wertvollen Platz, senken die
Aufenthaltsqualität und führen immer wieder zu Unfällen. Bis auf Liefer- und
Taxiverkehr sowie Anwohner\*innen soll daher unsere Innenstadt zur autofreien
Zone erklärt werden. Die dann nicht mehr benötigten Parkplätze sollen nach den
Ergebnissen eines stadtweiten Ideenwettbewerbs sinnvoll umgewidmet werden.

### b) Ambitionierter Ausbau der Radinfrastruktur

* in der ganzen Stadt muss ein umfangreiches Radvorrangnetz entstehen 
* Es muss mehr Radwege geben. Diese sollen sicher für jede/*n Verkehrsteilnehmer/*in sein, sodass keine Konfliktsituationen entstehen können. 
* Radschnellwege: Gut ausgebaute und direktere Verbindungsradwege zwischen den Orten in der Region 
* Mehr sichere Abstellmöglichkeiten, etwa auch im Marienplatzparkhaus 

Ein Anfang hierfür wären gezielte Markierungen und Beschilderungen. Beispielsweise könnten Straßen als Fahrradstraßen deklariert werden und Schutzstreifen für Radfahrende aufgemalt werden. 

### c) Verbesserung ÖPNV 

Starker Ausbau des ÖPNV. 
Auch in den ländlichen Raum, 
Ein Park and Ride solltevor der Wangenerstr. entstehen mit der Möglichkeit auch bspw. TWS Räder zu mieten.
Auch durch die Nutzung von Mobilitätsapps können umweltfreundliche Routen bzw. Verkehrsmittel ermittelt werden. Ein Vorbild hierfür ist Trafi: [www.trafi.com](https://www.trafi.com)

### d)  Rückbau der Autoinfrastruktur 

Es reicht nicht nur die Rad- und ÖPNV-Infrastruktur auszubauen, dann umso mehr Menschen vom ihre Autofahrten reduzieren umso Lehrer werden die Straßen. Daraus resultiert, dass es am Ende genausoviel Autoverkehr wie vorher gibt. *(zitat)* So führt eine Umwandlung von Auto Straßen, mit mehreren Spuren in jede Richtung, hinzu jeweils einer Autospuren pro Richtung langfristig zu einer Reduzierung des privaten Autoverkehr, wenn parallel auch Sozialgerechte alternativen geschaffenen werden.
Einfach und schnell umsetzbare Alternativen sind beispielsweise die Umwandlung von Autospuren im Bus- und Fahrradspuren.

Egal ob ÖPNV oder motorisierter Individualverkehr: Das Resultat von mehr Verkehr ist mehr Verkehr. 

Autofahren muss unattraktiver werden, damit Umsteigen auf klimafreundlichere Bewegungsmittel einen Sinn macht. Umso mehr Menschen auf ÖPNV umsteigen, umso attraktiver wird Auto fahren. Ohne Anreize gegen das Auto freuen sich Autofahrer*innen über jeden Menschen der umgestiegen ist. Schließlich bedeutet das: Mehr Parkplätze usw. 
Ein Art des Entgegenwirkens hierzu könnte sein, die Parkplätze in der Innenstadt teurer zu gestalten. 

Der Baubürgermeister Bastin hat sich hierzu in der Zeitung geäußert. Schwäbische Zeitung: "Außerdem soll ab 2030 jeder Autofahrer nur noch ein Drittel so viel durch die Gegend kurven wie jetzt. Das könne nur funktionieren wenn man die Stadt attraktiver für Busnutzer, Radler und Fußgänger macht - und zugleich unattraktiv für Autofahrer." 

Alle öffentlichen Parkplätze in der Stadt müssen kostenpflichtig werden. Kostenlose Parkplätze für Autos sind eine sehr teure Subvention des Autoverkehrs. Besonders wichtig wäre es, das Parken auf dem Parkplatz der Oberschwabenhalle kostenpflichtig zu machen. 

### e) Politik der kurzen Wege

Letztendlich ist Verkehr vermeiden der einzige Weg wenn die Mobilität gleichzeitig erhalten werden soll die verminderung der Wege die jede*r einzelne zurücklegen muss. Z.B. durch eine dezentrale Versorgung mit kleinen Läden.


## 3. Altdorfer Wald erhalten

27 ha des Altdorfer Waldes soll aufgrund von Kiesarbeiten gerodet werden. Der Altdorfer Wald ist die grüne Lunge Oberschwabens, größtes Naherholungsgebiet im Landkreis und einer der Hauptgestalter des örtlichen Klimas

Eine Durchführung der Kiesabbaupläne im Altdorfer Wald würde bedeuten:
Abholzung von mind. 27 ha Wald (entspricht der Größe von über 50 Fußballfeldern) und Beseitigung von Waldboden, der ein unverzichtbarer Speicher für Wasser, Schadstoffe und CO² ist
Gefährdung der einmalig reinen Quellwasservorkommen in Vogt und Weissenbronnen für die mögliche Versorgung von bis zu rund 100.000 Menschen. Zusätzlich Speisung der Trinkwasserversorgungen von Waldburg, Schlier und der TWS (Weingarten und Ravensburg)
Zerstörung der durch Eiszeiten geschaffenen Geoformationen und Moränenlandschaft (Waldburger Rücken); Höhenzüge von bis zu 50 m werden unwiederbringlich verschwinden und Abbaugruben von 40 m Tiefe (= Gebäude mit 12 Stockwerken) mit Abbruchkanten bis zu 90 m entstehen

Es wäre ein brachialer Einschnitt in die Pflanzen- und Tierwelt, sowie ein
Wegfall des für die seelische und körperliche Gesundheit so wichtigen Naherholungsgebietes
Auch würde es bedeuten, dass der Schwerlastverkehr weiter zunehmen würde und die in den angrenzenden Gemeinden durch Zu- und Abtransport durch unzureichenden Verkehrswegen fahren würde. 

Den Abbau dieser einmaligen Kulturlandschaft zur Steigerung der Exportgewinne der Kiesindustrie für Österreich und die Schweiz verurteilen wir gezielt und halten es für streng verwerflich. Wir verlangen, dass der Altdorfer Wald geschützt und nicht gerodet wird. 

Quelle:https://www.openpetition.de/petition/online/hilf-mit-den-altdorfer-wald-zu-schuetzen

## 4. Regionalplan an Wissenschaftliche Forderungen anpassen 

### a) Klimagerechte Verkehrswende 
Klimafreundliche Energie- und Verkehrswende – mehr Bus- und Bahn-Verbindungen, Fahrrad- und Fußgängerwege.

### b) Lokaler Wald- und Trinkwasserschutz
Einen regionalen Grünzug „Altdorfer Wald“ – auch in der für den Wasserschutz und die Biotopvernetzung so wertvollen „Südhälfte“ – und als Startschuß für ein Landschaftsschutzgebiet ohne Kies- und Torfabbau.

### c) Zukunftsgerechter Umgang mit Ressourc
Weniger Energie- und Rohstoffverbrauch. Förderung erneuerbarer Energien.

### d) Keine Bodenversiegelung 
Ein Wachstum in der Region können wir durch keine Maßnahmen ausgleichen, denn wir müssen z.B. den CO2 Ausstoß pro Jahr um min 13% Prozent reduzieren um mit 50% Wahrscheinlichkeit das schlimmste zu verhindern. Jedes Wachstum macht das Erreichen der Klimaziele noch unrealistischer!

### e) Konsequenter Artenschutz
Stärkung der Biodiversität, da es einen massiven Rückgang an Insekten in Baden- Württemberg gibt. Bei der Vogel-Population sind es im Bodenseeraum minus 25%.
 
### f) Naturschutzgerechte Landwirtschaft 
Mehr Vorrangflächen für Natur-, Boden- und Wasserschutz sowie nachhaltige Landwirtschaft.


## 5. Öffentliche Positionierung zu einer angemessenen Verschärfung des Klimaschutzgesetzes von Baden-Württemberg

Die Baden-Württembergische Landesregierung hat sich das Ziel gesetzt, bis 2030 42 % weniger Treibhausgasemissionen im Vergleich zu 1990 auszustoßen. 
Damit würde sie nicht einmal die Ziele der EU einhalten, welche bis 2030 ... weniger Ausstoß vorschreiben.
Wir fordern eine offizielle Positionierung gegen dieses Vorgehen. 
