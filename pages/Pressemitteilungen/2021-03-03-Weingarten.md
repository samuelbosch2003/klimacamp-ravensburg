---
layout: page
title:  "3.3.2021: Klimagerechtigkeitsaktivist*innen hissen Banner vom Dach des Kuko Weingarten anlässlich Regionalplanbesprechung"
date:   2021-03-03 04:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 19
---

*Pressemitteilung des Ravensburger Klimacamps am 3. März 2021*

# Klimagerechtigkeitsaktivist\*innen hissen Banner vom Dach des Kuko Weingarten anlässlich Regionalplanbesprechung

Sperrfrist Mittwoch (3.3.2021) 15:00 Uhr
{: .label .label-red }

Am heutigen Mittwoch (3.3.2021) tagt der Gemeinderat Weingarten im Kuko
Weingarten (Kultur- und Kongresszentrum Oberschwaben). Dabei geht es um
den aktuellen Regionalplanentwurf, von dem ein wissenschaftliches
Gutachten kürzlich statistische Fehler und grundsätzliche
Inkompatibilität mit Klimazielen nachwies [1].

"Aus der Zivilgesellschaft gibt es breiten Widerstand gegen den
Regionalplanentwurf", erklärt Kreativaktivist Jonathan Oremek (16).
"Straßenneubauprojekte, Waldrodungen und Flächenversiegelung -- das hat
der Regionalverband allen Ernstes vor! In Zeiten der Klimakrise!"

Aus diesem Grund werden am heutigen Mittwoch (3.3.2021)
Klimagerechtigkeitsaktivist\*innen aus dem Umfeld des Ravensburger
Baumhausklimacamps um 14:00 Uhr, eine Stunde vor Sitzungsbeginn, am Kuko
Weingarten ein mahnendes Banner anbringen. "Wir möchten die
Politiker\*innen damit an ihre Verantwortung erinnern", so Oremek.

"Wir hoffen, dass alle betroffenen Gemeinden noch rechtzeitig den
Entwurf des undemokratischen Regionalverbands zurückweisen", ergänzt
Oremeks Mitstreiterin Rosina Kaltenhauser (17). Dabei beziehen sich die
Aktivist\*innen darauf, dass von den 56 Mitgliedern der
Verbandsversammlung nur sieben Frauen sind, CDU und FWV stärkste Kraft
bilden und damit die tatsächlichen Mehrheitsverhältnisse in den
Gemeinderäten gar nicht abgebildet werden [4]. Die endgültige Entscheidung wird
der Weingartener Gemeinderat am 22.3.2021 treffen.

"Den betroffenen Anwohner\*innen können wir aber versprechen: Durch
unsere Besetzung des Altdorfer Walds werden wir so oder so die
gewaltsame Rodung ihres Walds verhindern. Der Altdorfer Wald wird
weiterhin ein friedfertiger Ort bleiben, Trinkwasser für die Region
bereitstellen und Hunderttausende von Tonnen an Staub und CO₂ binden",
so Kaltenhauser weiter.

Das Hausdach wird durch die Aktion nicht beschädigt.

[1] https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf<br>
[2] https://www.schwaebische.de/landkreis/landkreis-ravensburg/amtzell_artikel,-regionalplan-sorgt-fuer-unmut-bei-klimaaktivisten-_arid,11332447.html<br>
[3] https://www.wochenblatt-news.de/klimaaktivisten-hissen-banner-im-amtzell/<br>
[4] https://www.rvbo.de/Verband/Gremien-des-Regionalverbandes-Bodensee-Oberschwaben


## Hinweis

Es ist nicht auszuschließen, dass ein etwaiger präventiver Polizeieinsatz die
Aktion verhindert. Eine ähnliche Aktion fand nämlich am 24.2. in Amtzell statt
(Schwäbische und Wochenblatt-News berichteten [2,3]). Aktuelle Informationen
über den Verlauf der Aktion gibt Ingo Blechschmidt (+49 176 95110311).
