---
layout: page
title:  "9.3.2021: Stadt kassiert erste rechtliche Schlappe in ihrem Kampf gegen Ravensburgs Klimagerechtigkeitsaktivist*innen"
date:   2021-03-09 10:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 20
---

*Pressemitteilung des Ravensburger Klimacamps am 9. März 2021*

# Stadt kassiert erste rechtliche Schlappe in ihrem Kampf gegen Ravensburgs Klimagerechtigkeitsaktivist\*innen

Nachdem die Schwäbische Zeitung am 23.12.2020 über erhebliche
Baumfällarbeiten am Bahndamm in Ravensburg berichtete [1], meldete
Klimacamper Kim Schulz für den darauffolgenden 24.12. eine Protestversammlung am
Fuße des damaligen ersten Baumhauses an. Das Ravensburger Ordnungsamt
sah darin ein Verstoß gegen das Versammlungsgesetz und ergriff
rechtliche Schritte. Die Staatsanwaltschaft stellte das Verfahren nun
aber ein. "Die Stadt Ravensburg erlitt heute in ihrem Kampf gegen uns
Klimagerechtigkeitsaktivist\*innen einen ersten rechtlichen Rückschlag",
deutet Klimacamperin Rosina Kaltenhauser (17) die Verfahrenseinstellung.

Ein Klimacamper hatte die Protestversammlung ordnungsgemäß am 23.12., direkt
nach Bekanntwerden der Baumfällungen, angezeigt. Das Bundesverfassungsgericht
urteilte schon 1988, dass bei solchen Eilversammlungen die sonst nötige
48-Stunden-Anmeldefrist entfalle [2]. "Die Stadt kannte offensichtlich aber
nicht die Rechtsprechung des Bundesverfassungsgerichts und forderte uns auf,
die Versammlung nicht durchzuführen", erklärt Kaltenhauser. "Falls Sie die
Veranstaltung trotzdem durchführen begehen Sie eine Straftat, die wir zur
Anzeige bringen werden", schüchterte damals der Leiter des Ordnungsamts, Lothar
Kleb, Kim Schulz per E-Mail ein.

"Euch ist schon klar, dass das so nicht stattfinden darf", tönte
entsprechend einer der Polizeibeamt\*innen gleich zu Beginn, und begann,
Personalien der Versammlungsteilnehmer\*innen aufzunehmen. "Ich war so
eingeschüchtert, dass ich lieber ging. Ich verstehe nicht, was heute
vorfiel", gab damals Thomas Schmukal (34) aus Wangen an. "Dabei ist zum
Schutz der Versammlungsfreiheit gesetzlich verankert, dass Personalien
auf Versammlungen nur bei besonderer Gefahr für die öffentliche
Sicherheit aufgenommen werden dürfen", so Kaltenhauser.

Die Ravensburger Staatsanwaltschaft stellte gestern das Verfahren gegen
den Versammlungsanmelder nach § 153 StPO ein. "Diese Entscheidung
bestärkt uns in unserem Einsatz für Klimagerechtigkeit", so
Kaltenhauser. "Wir werden uns auch in Zukunft gegen die Kriminalisierung
unseres Engagements wehren. Auch unsere Besetzung des Altdorfer Walds
ist von der Versammlungsfreiheit geschützt."


## Hinweis

Mit der heute erfolgten Einstellung sind noch acht weitere Verfahren
gegen Klimacamper\*innen anhängig.

1. In fünf Fällen handelt es sich um vorgeworfene Verstöße gegen die
   Corona-Verordnung. "Mir wirft die Stadt vor, gegen die Corona-Verordnung
   verstoßen zu haben, obwohl ich mich bei mir zu Hause aufhielt", so
   Kaltenhauser. In zwei anderen Fällen gibt die Stadt von den beiden
   Aktivist\*innen an, sich in vier bzw. acht Meter Höhe im Baum befunden zu
   haben. "Vier Meter Abstand sind weit mehr als die zur Sicherheit
   erforderlichen 1½ Meter", erklärt Kaltenhauser. "Unsere Baumhäuser in
   Ravensburg waren ebenso wie die aktuelle Besetzung des Altdorfer Walds
   Corona-sichere Formen des Protests. Ist der Einsatz für den Erhalt
   unserer Lebensgrundlagen wirklich kein zulässiger Grund, die Wohnung
   trotz Ausgangssperre zu verlassen?"

2. In zwei weiteren Verfahren geht es um die Banneraktionen in Amtzell
   [3,4] und Weingarten [5].

3. Schließlich ist noch das Verfahren wegen der Räumungskosten in Höhe
   von 4.053,50 Euro anhängig.


## Referenzen

[1] https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-am-bahndamm-in-ravensburg-wurden-zig-baeume-radikal-gestutzt-_arid,11309390.html<br>
[2] 1 BvR 850/88, oder ganz banal Wikipedia: https://de.wikipedia.org/wiki/Artikel_8_des_Grundgesetzes_f%C3%BCr_die_Bundesrepublik_Deutschland#Anmeldepflicht<br>
[3] https://www.wochenblatt-news.de/klimaaktivisten-hissen-banner-im-amtzell/<br>
[4] https://www.schwaebische.de/landkreis/landkreis-ravensburg/amtzell_artikel,-regionalplan-sorgt-fuer-unmut-bei-klimaaktivisten-_arid,11332447.html<br>
[5] https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-klimaaktivisten-protestieren-auf-vordach-des-kongresszentrums-in-weingarten-_arid,11335856.htm
