---
layout: page
title:  "24.12.2020: Unverständnis über polizeiliche Einschüchterung bei Klimagerechtigkeitskundgebung"
date:   2020-12-24 19:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 5
---

*Pressemitteilung vom Ravensburger Klimacamp am 24. Dezember 2020*

# Unverständnis über polizeiliche Einschüchterung bei Klimagerechtigkeitskundgebung

Wie angekündigt, fand am heutigen Heiligabend beim Ravensburger Klimacamp eine
angemeldete Klimagerechtigkeitskundgebung statt. In den Grünanlagen zwischen
Stadtmauer und Schussenstraße protestieren etwa 30 Teilnehmer\*innen dafür,
dass die Stadt in Sachen Umwelt- und Klimapolitik Verantwortung übernimmt, und
spielten passende Stücke als Weihnachtskonzert für Klimagerechtigkeit, den Baum
und seine Bewohner\*innen. Alles lief geordnet und konform mit geltendem Recht
sowie Corona-Regeln ab, deren Einhaltung für die Aktivist\*innen sehr wichtig
ist.

Doch obwohl die Eilversammlung ordnungsgemäß angemeldet worden war und weder
ein Verbot noch weitergehende Auflagen vom Ordnungsamt erlassen worden waren,
versuchte die Polizei, die Versammlung im Vorhinein zu unterbinden. "Euch ist
schon klar, dass das so nicht stattfinden darf", tönte einer der
Polizeibeamt\*innen gleich zu Beginn. "Die Polizei drohte dann ohne rechtliche
Grundlage mit Ordnungswidrigkeits- und Strafanzeigen und nahm Personalien von
allen umstehenden Menschen auf", berichtet Aktivistin Mia (18). Dabei ist zum
Schutz der Versammlungsfreiheit gesetzlich verankert, dass Personalien auf
Versammlungen nur bei besonderer Gefahr für die öffentliche Sicherheit
aufgenommen werden dürfen.

"Ich war so eingeschüchtert, dass ich lieber ging. Ich verstehe nicht, was
heute vorfiel“, gab Thomas Schmukal (34) aus Wangen an. Andere Bürger\*innen,
die ebenfalls an der Versammlung teilnehmen wollten, taten es Schmukal gleich.
"Bisher erachtete ich die Polizei immer als Freund und Helfer", gab eine von
ihnen an. Sie möchten nach der Aufregung ihren Namen nicht auch noch in der
Zeitung lesen.

Auch das "Bäumchen-Kollektiv", das den Großteil der künstlerischen und
rednerischen Beiträge lieferte, war schockiert über das Vorgehen der Polizei:
"Diese Versuche, Verweise auf städtische Versäumnisse beim Umweltschutz von
vornherein zu ersticken, finden wir skandalös", so Kim Schulz, Leiter der
heutigen Versammlung und Teil der Musiker\*innengruppe "Das
Bäumchen-Kollektiv".

"Wir erwarten von Oberbürgermeister Rapp eine Erklärung für die heutige
polizeiliche Einschüchterung", findet Mia. "Gerade an Weihnachten zeugt das
heutige Vorgehen von keinerlei Maß."
