---
layout: page
title:  "17.12.2020: Gespräch mit Bürgermeister Blümcke, Forderungen und Straßentraverse"
date:   2020-12-17 09:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 3
---

*Pressemitteilung vom Ravensburger Klimacamp am 17. Dezember 2020*

# Gespräch mit Bürgermeister Blümcke, Forderungen und Straßentraverse

Das Ravensburger Baumhausklimacamp steht nun schon seit sechs Tagen und erhält
überregionalen Besuch. Zu der angekündigten polizeilichen Räumung kam es nicht.

"An Tag 4 hätte es eigentlich zu einem Gespräch mit Bürgermeister Simon Blümcke
kommen sollen", erzählt Klimaaktivistin Mia (18). "Wir waren dann aber etwas
enttäuscht, dass Herr Blümcke ein angekündigtes Gespräch fünf Minuten nach
angedachtem Beginn absagte und nicht erschien." Die Aktivist\*innen hatten extra
Vertreter\*innen verschiedener Initiativen eingeladen, um eine vielfältige
Diskussion zu ermöglichen.

An Tag 5 veröffentlichte die Baumbesetzung einen ersten Entwurf ihrer
Forderungen an die Stadt Ravensburg und den Landkreis Ravensburg.
Übergeordneter Appell ist die verbindliche Einhaltung eines CO2-Restbudgets,
das zumindest mit einer ⅔-Wahrscheinlichkeit als Ravensburger Anteil für eine
Beschränkung der Erderhitzung auf 1,5 Grad angemessen ist. Momentan plant die
Stadt, das ihr zustehende CO2-Budget um mehr als das Doppelte zu überschreiten;
der Landkreis hat sogar gar kein Ziel zur Klimaneutralität [1]. "Mit welchem
Recht nimmt sich unsere Regierung eine so erhebliche Überschreitung ihres
CO2-Budgets heraus?", fragt Mias Mitstreiter Samuel Bosch (17). "Andere Städte
wie Erlangen oder Rostock gehen mit gutem Beispiel voran und haben in ihren
Klimazielen keine so gewaltige Ambitionslücke wie Ravensburg verankert."

Ravensburg soll unter anderem die Radinfrastruktur ambitioniert ausbauen,
Bundesstraßen im Stadtgebiet rückbauen, den öffentlichen Personennahverkehr
ausbauen und vergünstigen sowie den Altdorfer Wald erhalten. "Auch der
Regionalplan bedarf dringend einer der Klimakrise angemessenen Revision."
Die Aktivist\*innen stellen auch Forderungen ohne Kostenfolge für Ravensburg:
"Ravensburg soll sich öffentlich sowie in den entsprechenden Gremien für eine
angemessene Verschärfung des Klimaschutzgesetzes von Baden-Württemberg
aussprechen. Momentan sieht dieses nicht einmal die Einhaltung der von
wissenschaftlicher Seite vielfach kritisierten EU-Klimaschutzziele vor."

"Unsere Forderungen entstehen in einem offenen, hierarchiefreien Prozess, und
alle Interessierten sind eingeladen, sie gemeinsam mit uns mitzugestalten",
lädt Bosch ein. Der Einladung folgten bereits Ortsgruppen von Fridays for
Future und Parents for Future in Ravensburg, Bodensee und Wangen, der Verein
zum Erhalt des Altdorfer Walds sowie Expert\*innen von Scientists for Future.

Mia stellt weitere Pläne der Aktivist\*innen vor: "In den nächsten Tagen spannen
wir eine Traverse über die angrenzende Straße. Ein sicher befestigtes großes
Banner wird dann allen untendurch fahrenden Autofahrer\*innen zeigen, dass die
Regierung ihrer Verantwortung nicht gerecht wird und nach wissenschaftlichen
Maßstäben zu wenig dafür tut, die Lebensgrundlage und Wirtschaft der
zukünftigen Generationen zu schützen." Außerdem kündigten weitere
Klimagerechtigkeitsaktivist\*innen aus Hessen und Bayern ihren Besuch im
Ravensburger Baumhausklimacamp an.

[1] https://ravensburg.klimacamp.eu/co2-budget/
