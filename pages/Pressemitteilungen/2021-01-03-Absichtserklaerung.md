---
layout: page
title:  "3.1.2021: Gemeinsame Absichtserklärung von Ravensburger Klimacamp und Polizei"
date:   2021-01-03 15:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 10
---

*Pressemitteilung vom Ravensburger Klimacamp am 3. Januar 2021*

# Gemeinsame Absichtserklärung von Ravensburger Klimacamp und Polizei

Wie der dpa-Meldung vom 31.12.2020 zu entnehmen war, suchte Polizeipräsident
Uwe Stürmer nach der am 29.12.2020 erfolgten Räumung des alten Baumhauses und
der am 30.12.2020 erfolgten Errichtung des neuen Baumhauses mit den
Ravensburger Klimacamper\*innen das Gespräch und bot Verhandlungen an. Dieses
Angebot nahmen die Aktivist\*innen gerne an. Schülerin Emma Junker (17) sprach
bereits in der dpa-Meldung von einem positiven Dialog und erklärte: "Wir wollen
uns ja nicht gegen die Polizei stellen, sondern gegen die Klimapolitik."

Der vollständige Vereinbarungstext ist
[online](/absichtserklaerung-bis-2021-01-10.pdf).

Kurz zusammengefasst: Bis zum 10.1. sichert die Polizei unser Grundrecht auf
Demonstration; wir dürfen unser Baumhaus betreiben.


## Wichtig

Entgegen anders lautender Berichte ist in der gemeinsamen Erklärung
ausdrücklich vereinbart, dass Banner und Transparente am Baum befestigt werden
können. Das Klimacamp willigte aber ein, auf Banner und Seile zwischen
verschiedenen Bäumen bis zum 10.1. zu verzichten.


## Zukunft

Nach dem 10.1. soll es zu Verhandlungen mit der Stadt kommen -- dies erhofften
wir uns schon seit Tag 1 des Baumhausklimacamps am 12.12.2020.
