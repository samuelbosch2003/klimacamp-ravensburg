---
layout: page
title:  "14.1.2021: Gespräch mit Stadt heute Abend -- Irritationen im Vorfeld"
date:   2021-01-14 11:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 11
---

*Pressemitteilung vom Ravensburger Klimacamp am 14. Januar 2021*

# Gespräch mit Stadt heute Abend -- Irritationen im Vorfeld

Am heutigen Donnerstag (14.1.) folgen Aktivist\*innen des Ravensburger
Klimacamps einer Einladung von Oberbürgermeister Rapp, um mit ihm, den
Bürgermeistern Blümcke und Bastin, Vertreter\*innen der Fraktionen,
Vertreter\*innen des Schüler\*innenrats und von Rapp bestellter Presse ein
Gespräch zu führen.

"Nachdem wir seit Beginn unseres Baumhausklimacamps am 12.12.2020 mit der Stadt
ins Gespräch kommen wollten, freuen wir uns, dass es nun endlich klappt",
erklärt Aktivist Samuel Bosch (18). "Unser größtes Ziel für heute Abend besteht
darin, in Erfahrung zu bringen, wieso bisher keiner der Beschlüsse des im Juli
letzten Jahrs einstimmig beschlossenen 'Klimakonsens' umgesetzt wurde." Nicht
einmal der Klimarat, der die Umsetzung begleiten sollte, wurde eingesetzt.
"Dabei würde diese Maßnahme nicht mal die Stadtkasse belasten."

Das Gespräch findet mit etwa 15 Menschen in einem geschlossenen Raum statt.
"Wir dachten ursprünglich, Herr Rapp würde einen echten inhaltlichen Austausch
mit uns suchen", fährt Bosch fort. "Einen solchen Austausch hätten wir uns
gewünscht. Bei der Vielzahl geladener Gäste müssen wir nun leider davon
ausgehen, dass das Treffen eher einer Inszinierung eines grünen Scheins dient." 

Irritationen gab es auch bei der Wahl des Gesprächsorts. Die
Klimagerechtigkeitsaktivist\*innen erachten beim derzeitigen Pandemiegeschehen
ein solch großes Treffen in einem geschlossen Raum für unverantwortlich.
"Mehrmals baten wir daher Herrn Rapp, das Gespräch als Videokonferenz
stattfinden zu lassen", kritisiert Bosch. "Die Stadt entblößt hier ihre
Doppelmoral, denn uns wirft sie ja immer wieder Verstöße gegen die
Corona-Verordnung vor."

Getreu ihrem grundlegenden Motto "Hört auf die Wissenschaft" war es den
Klimacamper\*innen sehr wichtig, Professor Ertel mitnehmen zu dürfen. "Doch
obwohl wir der Stadt erklärten, dass Herr Ertel bis 19:15 Uhr Vorlesung hat und
daher erst ab 19:30 Uhr kommen kann, beharrte die Stadt auf einen Beginn um
19:00 Uhr." Herr Rapp warf den Aktivist\*innen im Zuge der Terminabsprache sogar
vor, am Gespräch nicht ernsthaft interessiert zu sein. "Dabei möchten wir
lediglich einen inhaltlich versierten Experten auf unserer Seite haben, der mit
den Politiker\*innen auf Augenhöhe kommunizieren kann."

Schlussendlich blieb es bei 19:00 Uhr. "Wir kommen dann um 19:30 Uhr zusammen
mit Professor Ertel nach." Davor wird die Stadt also lediglich mit dem
Schüler\*innenrat sprechen (der in die Baumbesetzung nicht involviert ist).
