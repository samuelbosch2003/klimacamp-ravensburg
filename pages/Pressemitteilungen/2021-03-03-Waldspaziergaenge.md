---
layout: page
title:  "3.3.2021: Waldbesetzer*innen organisieren regelmäßige Waldspaziergänge und Austauschrunden für Bürger*innen"
date:   2021-03-03 02:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 18
---

*Pressemitteilung der Altdorfer Waldbesetzung am 3. März 2021*

# Waldbesetzer\*innen organisieren regelmäßige Waldspaziergänge und Austauschrunden für Bürger\*innen

Seit dem 25.2.2021 besetzen Klimagerechtigkeitsaktivist\*innen diverser
Initiativen nach den Vorbildern des Hambacher Forsts und des Dannenröder
Walds den Altdorfer Wald, um ihn vor der vom Regionalverbund geplanten
Rodung zu beschützen. Ab einschließlich dem 7.3.2021 organisieren die
Waldbesetzer\*innen nun jeden Sonntag um 14:00 Uhr öffentliche
Waldspaziergänge und Austauschrunden für interessierte Bürger\*innen.

"Der Regionalverband ist intransparent und undemokratisch", konstatiert
Waldbesetzerin Emma Junker (17). Dabei bezieht sie sich auf die
Tatsache, dass der für die Rodung verantwortliche Regionalverband
Statistiken über den Kiesexport unter Verschluss hält [1], bisher stets
zutreffende Prognosen des Statistischen Landesamts zum
Bevölkerungswachstum begründungslos diskreditiert [2] und von 56
Mitgliedern der Verbandsversammlung nur sieben Frauen sind und
Bürgemeister und Altbürgermeister von CDU und FWV stärkste Kraft bilden
[3]. "Diese Zusammensetzung bildet die tatsächlichen
Mehrheitsverhältnisse in den Gemeinderäten gar nicht ab!", so Junker.

"Dem setzen wir ein radikales Alternativprogramm entgegen: Wir laden ab
sofort jeden Sonntag alle interessierten Bürger\*innen ein, bei einem
Waldspaziergang für eine offene Austauschrunde zu uns ins Baumhausdorf
zu kommen", so Junker weiter. Viele Anwohner\*innen kündigten bereits
vergangene Woche ihre langfristige Unterstützung an und versorgen die
Waldbesetzung mit Mahlzeiten und Baumaterial. "Aber auch alle anderen
Nachbar\*innen sollen die Möglichkeit haben, uns kennenzulernen: Wir sind
eine bunte Mischung aus Schüler\*innen, Student\*innen, Auszubildenden,
Berufstätigkeiten und Vollzeitaktivist\*innen, die möchten, dass der Wald
der friedfertige Ort bleibt, der er aktuell noch ist." Im Anschluss an
die Austauschrunden finden für alle Interessierten unentgeltliche
Workshops zum aktivistischen Klettern und Baumhausbau statt. Junker:
"Eltern und Großeltern können gerne ihre Kinder und Enkel mitbringen."

Zu den offenen Austauschrunden sind auch Lokalpolitik und Presse
geladen. "Wir hoffen, Anwohner\*innen und Entscheidungsträger\*innen an
einen Tisch zu bringen", so Junkers Mitstreiter Lorenz Geiger (17).
"Vielleicht entwickeln unsere Politiker\*innen dann wieder ein
Verständnis für die Lebensgrundlagen der Anwohner\*innen und priorisieren
nicht länger die Profitgier eines einzelnen Kiesexportunternehmens. Die
natürliche Waldatmosphäre könnte helfen, dass diese Runden nicht zu den
sonst von der Politik bekannten Teufelskreisen degradieren."

[1] https://youtu.be/Mmp8FEuKCsc?t=287 (Zeitstempel 4:47)<br>
[2] https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf (Abschnitt 1.1)<br>
[3] https://www.rvbo.de/Verband/Gremien-des-Regionalverbandes-Bodensee-Oberschwaben


## Anfahrt

Auf dem Weg von Weingarten nach Wolfegg befindet sich das Baumhausdorf
kurz vor der Abzweigung Grund rechts im Wald. Parkplätze sind vorhanden.
Nah an der Besetzung hält die Buslinie 7534 (Haltestelle "Vogt Abzw.
Grund im Wald"). "Um Autofahrer\*innen auf das akute Politikversagen
hinzuweisen, spannen wir demnächst auch ein Transparent über die L317",
so Geiger. "Dann wird die Waldbesetzung ganz leicht zu finden sein."


## Aktueller Status

Die Waldbesetzung besteht seit dem 25.2.2021. Anlass sind die
Rodungspläne des Regionalverbands. "Obwohl wichtig für die
Trinkwasserversorgung und Staub- und CO₂-Bindung, soll der Wald einer
Kiesgrube mit 90 Meter hoher Abbruchkante weichen", erklärt Geiger den
Grund der Besetzung. "Dieser Zerstörungswut stellen wir uns entgegen."
Am 2.3. war der SWR lange für Filmaufnahmen im Wald. Mittlerweile gibt
es drei Wohnplattformen, die zu mehrstöckigen Baumhäusern ausgebaut
werden, ein Baumzelt und eine Materialplattform. Über Traversen und
Baumwipfelpfade (Walkways) sind die verschiedenen Orte des Baumhausdorfs
miteineinander verbunden. "Tagüber ist immer viel los", freut sich
Geiger über die vielen Unterhaltungen mit unterstützenden und
neugierigen Bürger\*innen. Die Anzahl Waldbewohner\*innen schwankt. In der
Nacht vom 2.3. auf den 3.3. sind es neun Aktivist\*innen.


## Fotos zur freien Verwendung

https://ravensburg.klimacamp.eu/altdorfer-wald/ (Urheber: Altdorfer
Waldbesetzung)

## Kontakt

Ingo Blechschmidt (+49 176 95110311) stellt gerne den Kontakt zu den
Waldbesetzer\*innen her. Es gibt keine autorisierte Gruppe und kein
beschlussfähiges Gremium, das "offizielle Gruppenmeinungen" für die
Besetzung beschließen könnte. Die Menschen in der Besetzung und ihrem
Umfeld haben vielfältige und teils kontroverse Meinungen. Diese
Meinungsvielfalt soll nicht zensiert werden, sondern gleichberechtigt
nebeneinander stehen. Kein Text spricht für die ganze Besetzung oder
wird notwendigerweise von der ganzen Besetzung gut geheißen.
