---
layout: page
title:  "24.2.2021: Klimagerechtigkeitsaktivist*innen besetzen Altdorfer Wald, um ihn vor Rodung zu schützen"
date:   2021-02-24 16:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 17
---

*Pressemitteilung vom Ravensburger Klimacamp am 24. Februar 2021*

# Klimagerechtigkeitsaktivist\*innen besetzen Altdorfer Wald, um ihn vor Rodung zu schützen

Sperrfrist Freitag (26.2.2021) 16:00 Uhr
{: .label .label-red }

**Pressekonferenz am Samstag (27.2.) um 11:00 Uhr im Wald**
mit Vertreter\*innen von:

1. Waldbesetzung
2. Natur- und Kulturlandschaft Altdorfer Wald e.V.
3. Aktionsbündnis Regionalplan
4. Parents for Future
5. Scientists for Future

Thema der Pressekonferenz: "Liebe CDU, lieber RVBO -- wir appellieren nicht
länger mit Demonstrationen an euer Verantwortungsbewusstsein. Ab sofort stellen
wir uns eurer Zerstörungswut aktiv in den Weg. Der Altdorfer Wald bleibt."


## Waldbesetzung

Seit dem 25.2021 besetzen Aktivist\*innen des Ravensburger Klimacamps bis auf Weiteres den
Altdorfer Wald. Auf Initiative des Unternehmens Meichle+Mohr sieht der
aktuelle Regionalplanentwurf des Regionalverbands Bodensee-Oberschwaben
die Rodung von großen Teilen des Walds vor. "Für die CDU und für den
Regionalverband ist der Altdorfer Wald nur ein bisher nicht vollständig
erschlossenes Kiesabbaugebiet. Für uns aber ist dieser wertvolle
Lebensraum ein Symbol für unsere Zukunft. Wir werden uns der
gewaltvollen Zerstörung des Altdorfer Walds friedlich in den Weg
stellen", verkündet Klimacamperin Emma Junker (17). "Der Altdorfer Wald
bleibt!"

Über die letzten Wochen errichteten die Aktivist\*innen bereits
verschiedene Strukturen wie Baumhäuser und Plattformen in drei bis fünf
Meter Höhe. Passend zum landesweiten Streik von Fridays for Future am
26.2.2021 (in Ravensburg um 14:00 Uhr beim Schussenpark beginnend) sind
diese bezugsfertig. Anwohner\*innen sicherten bereits ihre Unterstützung
in Form von warmen Essensangeboten, Wäsche und Materialien zu.

Die Besetzung des Altdorfer Walds hat die erfolgreichen Besetzungen des
Hambacher Forsts und des Dannenröder Walds zum Vorbild. "Dank der
Aktionen in den letzten Monaten sind wir mittlerweile überregional
bekannt. Daher wird unsere Besetzung zahlreiche
Klimagerechtigkeitsaktivist\*innen aus ganz Deutschland anziehen, Besuche
aus Augsburg und Berlin kündigten sich bereits an", so Junkers Kollege
Lorenz Geiger (17).

"Wenn CDU und RVBO schon keinen Respekt vor intakten Ökosystemen haben
und ihren unbezifferbaren Wert geringschätzen, dann ändern vielleicht
zumindest die nicht unerheblichen Polizeieinsatzkosten ihre kühlen
Profitkalkulationen", so Geiger weiter. Der Einsatz im Dannenröder Wald
kostete 150 Millionen Euro -- "genug, um den gesamten öffentlichen
Personennahverkehr in Hessen für vier Monate kostenlos zu machen" [2].
"Bürger\*innen, die sich ebenfalls für einen Erhalt des Altdorfer Walds
aussprechen, finden auf ravensburg.klimacamp.eu mehrere Petitionen zum
Thema."

[1] https://augsburg.klimacamp.eu/danni<br>
[2] https://klimaschutz.madeingermany.lol/<br>
[3] https://www.youtube.com/watch?v=Mmp8FEuKCsc

## Hintergrund zum Altdorfer Wald

Alexander Knor vom Verein [Natur- und Kulturlandschaft Altdorfer Wald
e.V.](https://altdorferwald.org/) erklärt in seinem Video [3]: "Der Altdorfer Wald,
das größte zusammenhängende Waldgebiet in Oberschwaben, ist einer der
entscheidenden Faktoren für eine lebenswerte Heimat im Landkreis
Ravensburg. Er liefert Sauerstoff zum Atmen, filtert Hunderttausende von
Tonnen an Staub und CO2, ist Helfer beim Klimaschutz, hat starken
Einfluss auf das regionale Klima, speichert Regenwasser in
unvorstellbaren Mengen, liefert Trinkwasser aller bester Qualität, ist
Naherholungsgebiet, Abenteuerspielplatz und ein wichtiger Faktor für die
Forstwirtschaft vor Ort. Durch die vorgestellten Planungen des
Regionalverbands Bodensee-Oberschwaben sind nicht nur einzelne dieser
Faktoren gefährdet, sondern der Altdorfer Wald in seiner gesamten
Funktion für die Region. Er ist erstaunlich und besorgniserregend, wie
Interessenverbände und Kieslobbyisten mit einem über 10.000 Jahre alten
Naturjewel umgehen und den systematischen Raubbau vorantreiben."


## Video

Ansage an CDU und RVBO:
https://ravensburg.klimacamp.eu/altdorfer-wald/


## Ort

Koordinaten der Besetzung: 47.810973, 9.76126 (nahe Bushaltestelle Vogt
Abzw. Grund im Wald)
