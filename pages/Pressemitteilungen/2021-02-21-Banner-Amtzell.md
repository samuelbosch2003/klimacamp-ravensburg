---
layout: page
title:  "21.2.2021: Klimagerechtigkeitsaktivist*innen befestigen Banner am Dach anlässlich Regionalplanbesprechung"
date:   2021-02-21 18:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 16
---

*Pressemitteilung vom Ravensburger Klimacamp am 21. Februar 2021*

# Klimagerechtigkeitsaktivist*innen befestigen Banner am Dach anlässlich Regionalplanbesprechung

Sperrfrist Montag (22.2.2021) 19:00 Uhr
{: .label .label-red }

Am morgigen Montag (22.2.2021) tagt der Gemeinderat Amtzell in der
Amtzeller Mehrzweckhalle. Dabei geht es auch um den aktuellen
Regionalplanentwurf, von dem ein wissenschaftliches Gutachten kürzlich
statistische Fehler und grundsätzliche Inkompatibilität mit Klimazielen
nachwies [1].

"Der aktuelle Regionalplanentwurf ist ein Klimahöllenplan!", so
Kreativaktivistin Johanna Wenz (17). "Deswegen werden wir ein Banner am
Gebäudedach befestigen. Wir möchten die anwesenden Politiker\*innen
ermutigen, eine kluge Entscheidung zu treffen. Für uns bedeutet diese
unglaublich viel."

"Unsere Regierungen verhöhnen die Wissenschaft konsequent und schätzen
unsere Zukunft gering. Daher sehen wir uns zu dieser Grenzüberschreitung
gezwungen." Das Hausdach wird durch die Aktion nicht beschädigt.

[1] https://site-1008701.mozfiles.com/files/1008701/S4F_Kritische-Wurdigung-Regionalplanentwurf-BO_Entwurf-mit-Anlagen_Endversion_11Feb2021-1.pdf
