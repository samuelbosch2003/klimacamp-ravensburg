---
layout: page
title: 🏕 Altdorfer Wald 🏕
permalink: /altdorfer-wald/
nav_order: 30
has_children: true
has_toc: false
---

# Liebe [CDU](https://www.cdu-kreis-rv.de/), lieber [RVBO](https://www.rvbo.de/) --

<div style="font-size: large">wir appellieren nicht länger an euer
Verantwortungsbewusstsein. <strong>Ab sofort stellen wir uns eurer gewaltvollen
Zerstörung selbst in den Weg.</strong> Der Altdorfer Wald bleibt.</div>

<video controls width="480" height="270" style="margin-top: 1em; width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)" preload="metadata" poster="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/altdorfer-wald.jpeg" title="Ansage an die CDU und den RVBO: Der Altdorfer Wald ist besetzt!">
  <source src="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/altdorfer-wald-small.mp4" type="video/mp4">
</video>

<img src="/altdorfer-wald-besetzt.jpeg" width="640" height="480" style="width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)" alt="Der Altdorfer Wald ist besetzt.">

### Was ist der Altdorfer Wald?

Der Altdorfer Wald ist das größte zusammenhängende Waldgebiet in Oberschwaben
und einer der entscheidenden Faktoren für eine lebenswerte Heimat im Landkreis
Ravensburg. Er liefert Sauerstoff zum Atmen, filtert Hunderttausende Tonnen
an Staub und CO₂, ist Helfer beim Klimaschutz, hat starken Einfluss auf das
regionale Klima, speichert Regenwasser in unvorstellbaren Mengen, liefert
Trinkwasser aller bester Qualität, ist Naherholungsgebiet, Abenteuerspielplatz
und ein wichtiger Faktor für die Forstwirtschaft vor Ort.
[Video](https://www.youtube.com/watch?v=Mmp8FEuKCsc)


### Ein wertvolles Ökosystem also. Aber?

Der aktuelle [Regionalplanentwurf](/regionalplan/) des
[Regionalverbands](https://www.rvbo.de/) sieht
auf Initiative des Unternehmens [Meichle+Mohr](https://www.meichle-mohr.de/) die Rodung von großen Teilen des
Altdorfer Walds vor. Er soll einer Kiesgrube (inklusive 90 Meter hoher
Abbruchkante) weichen. Der Kies wird übrigens nicht mal vor Ort verbaut,
sondern nach Österreich und die Schweiz exportiert, wo restriktivere
Umweltauflagen den Kiesabbau in die Schranken weisen. Es ist erstaunlich und besorgniserregend, wie
Interessenverbände und Kieslobbyisten mit einem über 10.000 Jahre alten
Naturjuwel umgehen und den systematischen Raubbau vorantreiben.


### Und nun?

Für die [CDU](https://www.cdu-kreis-rv.de/) und für den Regionalverband ist der
Altdorfer Wald nur ein bisher nicht vollständig erschlossenes Kiesabbaugebiet.
Für uns aber ist dieser wertvolle Lebensraum ein Symbol für unsere Zukunft.
**Wir stellen uns der gewaltvollen Zerstörung des Altdorfer Walds in den Weg!**
Bis auf Weiteres besetzen wir den Altdorfer Wald, um ihn vor der Rodung zu
schützen.

CDU und RVBO haben absolut keinen Respekt vor intakten Ökosystemen und
geringschätzen ihren unbezifferbaren Wert. Vielleicht ändern zumindest die nicht
unerheblichen Polizeieinsatzkosten ihre kühlen Profitkalkulationen. Der
Einsatz im [Dannenröder
Wald](https://de.wikipedia.org/wiki/Dannenr%C3%B6der_Forst) kostete [150
Millionen Euro](https://augsburg.klimacamp.eu/danni/) -- genug, um den gesamten
öffentlichen Personennahverkehr in Hessen [für vier Monate kostenlos zu
machen](https://klimaschutz.madeingermany.lol/). Wäre sinnvoller gewesen, oder?

Der Widerstand gegen die Rodung des Altdorfer Walds ist in einen größeren
Kontext eingebunden: Zahlreiche Initiativen und Verbände schlossen sich zu
einem Aktionsbündnis zusammen, um eine klima- und umweltfreundliche
Überarbeitung des aktuellen Regionalplanentwurfs einzufordern.

[Ich möchte die Besetzung unterstützen!](/altdorfer-wald/unterstuetzen/){: .btn .btn-purple }
[Ich möchte für eine Zeit im Wald wohnen!](/altdorfer-wald/leben/){: .btn .btn-purple }
[Der Regionalplan ist ein Klimahöllenplan](/regionalplan/){: .btn .btn-pink }
[Pressespiegel](/pressespiegel/){: .btn .btn-blue }
[Fotos](https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/#113){: .btn .btn-blue }

**Stand 8.4.2021 haben wir ein Defizit von etwa 3.000 Euro, das Privatperson
auslegten. [Wir sind daher über Spenden sehr dankbar.](/spenden/)**

<img src="/icons/telegram.svg" style="width: 1em; height: 1em">
**[Infokanal auf Telegram](https://t.me/altdorfer_wald)**
(hier teilen wir nur wichtige Nachrichten -- direkter Kontakt und Beteiligung
an der Organisation ist über die verknüpfte Diskussionsgruppe möglich)

☎️ **Waldtelefon:** [+49 176 95110311](tel:+4917695110311)

🗺 **Koordinaten:** 47.810973, 9.76126 ([Anreise](/altdorfer-wald/anreise/))

**Hinweis:** *Die Besetzung wurde zwar vom Ravensburger Klimacamp angestoßen,
diese Website wurde aber nicht mit der gesamten Besetzung abgesprochen. Es gibt
keine autorisierte Gruppe und kein beschlussfähiges Gremium, das "offizielle
Gruppenmeinungen" für die Besetzung beschließen könnte. Die Menschen in der
Besetzung und ihrem Umfeld haben vielfältige und teils kontroverse Meinungen.
Diese Meinungsvielfalt soll nicht zensiert werden, sondern gleichberechtigt
nebeneinander stehen. Kein Text spricht für die ganze Besetzung oder wird
notwendigerweise von der ganzen Besetzung gut geheißen.*
