---
layout: page
title: Pressespiegel
permalink: /pressespiegel/
nav_order: 110
---

# Pressespiegel
Das sagen die Anderen über uns (unvollständig):

**Siehe auch: [Leser\*innenbriefe](https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp-briefe)**

## 8.4.2021
* Schwäbische: [Klima-Aktivisten blockieren Kieswerk in Grenis](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-klimaaktivisten-blockieren-kieswerk-in-grenis-_arid,11349654.html)
* Ravensburger Spectrum: [Grenis/Altdorfer Wald: Aktivist/innen legen Aspahltwerk lahm](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2804097/grenisaltdorfer-wald-aktivistinnen-legen-aspahltwerk-lahm)

## 7.4.2021
* SWR Landesschau: [ab Zeitstempel 21:30](https://www.ardmediathek.de/video/swr-aktuell-baden-wuerttemberg/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzE0NDEwMTU/)
* Schwäbische: [2700 Beschwerden gegen Regionalplan für Oberschwaben](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-2700-beschwerden-gegen-regionalplan-fuer-oberschwaben-_arid,11349063.html)

## 2.4.2021
* Ravensburger Spectrum: [Altdorfer Wald: HUPEN für den KLIMASCHUTZ - "Regionalverband und CDU sollten sich schämen."](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2766508/altdorfer-wald-hupen-fur-den-klimaschutz%20---cdu-und-regionalverband-sollten-)

## 31.3.2021
* Ravensburger Spectrum: [Ravensburger Klimarat von hochdotierten Experten gemäß Gemeindeordnung Ba-Wü rechtlich nicht zulässig](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2753982/)

## 30.3.2021
* Schwäbische: [Streit um Kosten des künftigen Ravensburger Klimarats](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-streit-um-kosten-des-kuenftigen-ravensburger-klimarats-_arid,11346605.html)
* SatireSenf: [TS38/21: Regionalplan-Kritiker: Sonnenschein und subkutane Reden zum Petitionsstart – Aufruf zum zivilen Ungehorsam – Regionalpresse schweigt](https://satiresenf.de/ts38-21-regionalplan-kritiker-sonnenschein-und-subkutane-reden-zum-petitionsstart-aufruf-zum-zivilen-ungehorsam-regionalpresse-schweigt/)
* Ravensburger Spectrum: ["Wer auf die Politik v€rtraut, der hat auf Sand g€baut" - Molldietetunnel: Chefsache oder Phallussymbol?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2749105/wer-auf-die-politik-vrtraut---der-hat-auf-sand-gbaut-----oberschwabische-re)

## 29.3.2021
* Südwest Presse: [Protestcamp und Petition gegen Kiesabbau](https://www.swp.de/suedwesten/kiesabbau-in-oberschwaben-petitionen-protestcamp_-baumbesetzung-55989050.html) ([Foto](/pressespiegel/2021-03-29-swp.jpeg))
* Badische Zeitung: [15.000 Bäume sollen in einem Wald einer Kiesgrube weichen](https://www.badische-zeitung.de/mit-baumhaeusern-gegen-kiesbagger--200960663.html) ([Foto](/pressespiegel/2021-03-29-badische.jpeg))

## 24.3.2021
* Schwäbische: [Kiesabgabe rückt erstmal in weite Ferne](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-kiesabgabe-rueckt-erstmal-in-weite-ferne-_arid,11344465.html)
* Schwäbische: [Weingartens OB Ewald lehnt Kiesabbau im Altdorfer Wald ab](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-ewald-kein-kiesabbau-im-altdorfer-wald-_arid,11343442.html)

## 23.3.2021
* Ravensburger Spectrum: [Petition "Rauchende Schornsteine zu zeitgemäßen Bedingungen"](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2715566/petition-rauchende-schornsteine-zu-zeitgemasen-bedingungen)

## 22.3.2021
* Schwäbische: [Globaler Klimastreik auch in Ravensburg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-globaler-klimastreik-auch-in-ravensburg-_arid,11342433.html)

## 21.3.2021
* @traeumenvonbaeumen: [Instagram](https://www.instagram.com/tv/CMsX5rFANnm/)

## 18.3.2021
* Schwäbische: [Wissenschaftler: Status Quo verursacht allein in Ravensburg jedes Jahr Millionenschäden](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-wissenschaftler-status-quo-verursacht-allein-in-ravensburg-jedes-jahr-millionenschaeden-_arid,11341613.html)

## 15.3.2021
* Ravensburger Spectrum: [Einst haben die Kerls auf den Bäumen gehockt ... Stirbt der Baum - stirbt auch der Mensch !!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2693846/einst-haben-die-kerls-auf-den-baumen-gehockt---)

## 14.3.2021
* Ravensburger Spectrum: ["Parents for Future" laden zum Aufmarsch und Kundgebunge ein - Umweltthema darf nicht "untergehen"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2693235/parents-for-future-laden-zum-aufmarsch-und-kundgebunge-ein---umweltthema-da)

## 13.3.2021
* Ravensburger Spectrum: [Vollltext Stellungnahme des Petitionsausschusses in Sachen "Altdorfer Wald" (Petition aus 2019)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2690011/landtagswahl-kleine-entscheidungshilfe-gefallig-https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2690011/landtagswahl-kleine-entscheidungshilfe-gefallig-)
* Ravensburger Spectrum: [Ravensburger Klima: Guter Rat muss nicht teuer sein!](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2689321/ravensburger-klima-guter-rat-ist-teuer-)
* Ravensburger Spectrum: [Klimarat - Am Beispiel der Stadt Köln: Wie er funktionieren könnte ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2690536/klimarat-am-beispiel-der-stadt-koln-wie-er-funktionieren-konnte-)

## 12.3.2021
* Schwäbische: [Anzeige gegen Ravensburger Klima-Aktivisten wurde fallengelassen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-demo-an-besetztem-baum-in-ravensburg-war-nicht-rechtswidrig-_arid,11339421.html)

## 11.3.2021
* Schwäbische: Klimarat schaut der Stadt bald auf die Finger
* Ravensburger Spectrum: [Offizielle Dokumente beweisen: "Schuld" in SACHEN zu hohen KIESABBAUS in der REGION ist nicht der Regionalverband allein, sondern primär das GRÜNE UMWELTMINISTERIUM!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2682955/regierungsdokument-beweist-schuld-in-sachen-kiesabbau-in-der-region-ist-nic)
* Ravensburger Spectrum: [Bürger aus dem Kreis Ravensburg reicht Petition "FFH für den gesamten Altdorfer Wald" bei der EU ein](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2683430/ravensburger-burger-reicht-petition-ffh-im-altdorfer-wald-bei-der-eu-ein)

## 10.3.2021
* CrimethInc: [The Forest Occupation Movement in Germany](https://crimethinc.com/2021/03/10/the-forest-occupation-movement-in-germany-tactics-strategy-and-culture-of-resistance)
* Ravensburger Spectrum: [Interview mit einem BAUM, der für den LANDTAG in Stuttgart kandidiert](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2678950/interview-mit-dem-baum-der-fur-den-landtag-in-stuttgart-kandidiert)

## 9.3.2021
* ANF: [Stadt kassiert erste Schlappe gegen Ravensburger Klimacamp](https://anfdeutsch.com/Oekologie/ravensburger-klimacamp-stadt-kassiert-erste-schlappe-24943)
* Ravensburger Spectrum: [Die Stadt Ravensburg muss erste rechtliche Doppel-Schlappe in ihrem Kampf gegen Ravensburger Klimagerechtigkeitsaktivisten kassieren!!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2677349/die-stadt-ravensburg-muss-erste-rechtliche-doppel-schlappe-in-ihrem-kampf-g)

## 8.3.2021
* Schwäbische: [Regionalplan im Weingartener Gemeinderat: Katastrophe oder Zukunftsvision?](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-regionalplan-im-weingartener-gemeinderat-katastrophe-oder-zukunftsvision-_arid,11337787.html)

## 7.3.2021
* Schwäbische: [Umstrittener Kiesabbau im Altdorfer Wald: So stehen die Ravensburger Kandidaten dazu](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-der-altdorfer-wald-als-wahlkampfthema-_arid,11336876.html)
* Ravensburger Spectrum: [Neues aus dem "WÜHLKREIS 69" (Ravensburg/Tettnang) -- Wenn Satire und Ernst aufeinander treffen und wenn Kies zu "Kies" wird!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2672415/neues-aus-dem-wuhlkreis-69-ravensburgtettnang---wenn-satire-und-ernst-aufei)
* Ravensburger Spectrum: [Große Teile des ALTDORFER WALDS sind (!) seit JANUAR 2019 ALS "FAUNA-UND FLORA-SCHUTZGEBIET-FFH der EU" AUSGEWIESEN -- Proteste zu RECHT!!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2672728/alarm-altdorger-wald-ist-eu-fauna-und-flora-hab itat-und-seit-januar-als-sch)

## 5.3.2021
* Schwäbische: [Karikatur von Rainer Weishaupt](//pressespiegel/2021-03-02-schwaebische.jpeg): Fuchs und Has und dem ganzen Rest ist es einfach zu viel: Alle streiten sich um ihr Zuhause. Die Kieser wollen es platt machen und nach Rohstoff buddeln; die Aktivisten ziehen kurzerhand einfach selber ein, um dagegen zu protestieren. Bis klar ist, was jetzt wirklich mit dem Wald bei Grund passiert, ziehen die Tiere provisorisch schon mal um. Wenn da bloß mal kein Windpark kommt ...
* Regio TV: [Klimaaktivisten besetzen Altdorfer Wald](https://www.regio-tv.de/mediathek/video/klimaaktivisten-besetzen-altdorfer-wald/)
* Ravensburger Spectrum: [Die Geschichte des Altdorfer Waldes aus der Sicht eines Zeitzeugen aus der ersten Hälfte des 19. Jahrhunderts](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2667762/)

## 4.3.2021
* SWR Landesschau: [Protest gegen Kiesabbau im Altdorfer Wald](https://www.swrfernsehen.de/landesschau-bw/landesschau-baden-wuerttemberg-vom-432021-100.html)
* Schwäbische: [Waldbesetzer bekommen Zuwachs im Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-waldbesetzer-bekommen-zuwachs-_arid,11336277.html)
* Ravensburger Spectrum: [Ravensburger Bürger stellt seinen Bürgermeistern **21.315,84 Euro** in Rechnung !! // Verletzung EU-Umweltrecht (FFH) und Steuergeldermissbrauch](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2664720/ravensburger-burger-stellt-seinen-burgermeister n-2131584-euro-in-rechnung-)

## 3.3.2021
* Schwäbische: [Klimaaktivisten protestieren auf Vordach des Kongresszentrums in Weingarten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-klimaaktivisten-protestieren-auf-vordach-des-kongresszentrums-in-weingarten-_arid,11335856.html)
* Südfinder: Ravensburg macht Ernst! Sachbeschädigung: Aktivisten müssen 5.000 Euro zahlen ([Foto](/pressespiegel/2021-03-03-suedfinder.jpeg))
* ANF: [Besetzer des Altdorfer Walds laden ein zum offenene Austausch](https://anfdeutsch.com/Oekologie/besetzer-des-altdorfer-walds-laden-ein-zum-offenene-austausch-24808)
* Ravensburger Spectrum: [WEINGARTEN wird zu BAUMGARTEN - BANNERAKTION am KUKO - "Ihr da oben, wir da unten?" - Den Spieß umdrehen ...](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2661557/weingarten-wird-zu-baumgarten---banneraktion-am-kukoz)
* Ravensburger Spectrum: [Altdorfer Wald muss bleiben - Aktivist/innen besetzen Ort der Begehrlichkeiten](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2661092/altdorfer-wald-muss-bleiben---aktivistinnen-besetzen-ort-der-begehrlichkeit)

## 2.3.2021
* Schwäbische: [Hunderte von Beschwerden gegen Regionalplan gehen ein](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-hunderte-von-beschwerden-gegen-regionalplan-_arid,11335127.html)

## 28.2.2021
* Wochenblatt-Online: [Klimagerechtigkeitsaktivist\*innen besetzen Altdorfer Wald](https://wochenblatt-online.de/klimagerechtigkeitsaktivistinnen-besetzen-altdorfer-wald/)
* Ravensburger Spectrum: [Ravensburger Oberbürgermeister besetzt den "Hirschgraben 2" und hängt Banner auf -- Zeigen, wer Herr im Hause ist ! (Satire)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2652234/ravensburger-oberburgermeister-besetzt-den-hirschgraben-2---zeigen-wer-herr)

## 27.2.2021
* Schwäbische: [Bündnis gegen Kiesabbau macht sich im Altdorfer Wald breit](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-buendnis-gegen-kiesabbau-macht-sich-im-altdorfer-wald-breit-_arid,11334128.html)
* SWR Aktuell: [Klimaaktivisten besetzen Bäume im Altdorfer Wald](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/klimaaktivisten-besetzen-baeume-im-altdorfer-wald-100.html)

## 26.2.2021
* Schwäbische: [Ein zweiter Hambacher Forst? Aktivisten besetzen den Altdorfer Wald](https://www.schwaebische.de/landkreis/landkreis-ravensburg/vogt_artikel,-ein-zweiter-hambacher-forst-aktivisten-besetzen-den-altdorfer-wald-_arid,11333867.html)
* Schwäbische: [Fridays-for-Future-Aktivisten demonstrieren auf der Ravensburger Schussenstraße](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stockender-verkehr-auf-der-schussenstrasse-durch-klimaschuetzer-_arid,11333862.html)
* Ravensburger Spectrum: [Aus aktuellem Anlass: Warum Jugendliche tun, was sie tun -- Inklusive Ausflug in die Hirnforschung](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2646996/)
* Ravensburger Spectrum: [Klimagerechtigkeitsaktivist\*innen besetzen Altdorfer Wald](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2647139/)
* Ravensburger Spectrum: [Redebeiträge, Lieder, viel Beifall, friedliche "Schlachtrufe" und ein Banner: Fridays for Future mobilisiert 100 Bürger/innen](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2647963/redebeitrage-lieder-viel-beifall-friedlichen-schlachtrufe-und-ein-banner-fr)
* Wochenblatt-News: [Klimaaktivisten besetzen Altdorfer Wald](https://www.wochenblatt-news.de/klimaaktivisten-besetzen-altdorfer-wald/)

## 25.2.2021
* Schwäbische: [Fridays-for-Future-Aktivisten wollen Schussenstraße blockieren](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-fridays-for-future-aktivisten-wollen-schussenstrasse-blockieren-_arid,11333389.html)
* Ravensburger Spectrum: [Wer "GRÜN" wählt sollte nicht vergessen, dass auch GURKEN grün sind! // Zur Präsentation von Manne Lucha](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2644512/)
* Ravensburger Spectrum: [Ravensburger Fridays-for-Future-Aktivist\*innen blockieren die Ravensburger Schussenstraße mit Gehzeugen](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2645419/)

## 23.2.2021
* Schwäbische: [Regionalplan sorgt für Unmut bei Klimaaktivisten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/amtzell_artikel,-regionalplan-sorgt-fuer-unmut-bei-klimaaktivisten-_arid,11332447.html)
* Schwäbische: [Ravensburger Baumbesetzer erhalten Rechnung der Stadt](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-erhalten-rechnung-der-stadt-_arid,11332277.html)
* Wochenblatt-News: [Klimaaktivisten hissen Banner im Amtzell](https://www.wochenblatt-news.de/klimaaktivisten-hissen-banner-im-amtzell/)

## 22.2.2021
* Schwäbische: [Samuel Bosch und Wolfgang Ertel zu Gast beim Weingartener Jugendgemeinderat](https://www.schwaebische.de/landkreis/landkreis-ravensburg/weingarten_artikel,-samuel-bosch-und-wolfgang-ertel-zu-gast-beim-weingartener-jugendgemeinderat-_arid,11331805.html) ([Foto](/pressespiegel/2021-02-22-schwaebische.jpeg))
* Ravensburger Spectrum: [Ravensburger Baumcamper/innen in Amtzell: Statt in die eigenen Knie, gehen sie anderen auf's Dach ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2635581/)

## 20.2.2021
* Ravensburger Spectrum: [Juristisch ausgebildeter Bürgermeister stellt Ravensburger Baumcamper zu Unrecht Vollstreckungsbescheid zu](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2632396/juristisch-ausgebildeter-burgermeister-stellt-ravensburger-baumcamper-zu-un)

## 18.2.2021
* Schwäbische: [Wissenschaftler: Regionalplan für Region Bodensee-Oberschwaben passt nicht zu Klimazielen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-wissenschaftler-regionalplan-fuer-region-bodensee-oberschwaben-passt-nicht-zu-klimazielen-_arid,11330127.html) ([Foto](/pressespiegel/2021-02-18-schwaebische.jpeg))
* Schwäbische: [Ravensburger Baumbesetzer widersprechen der Stadtverwaltung](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-widersprechen-der-stadtverwaltung-_arid,11330327.html) ([Foto](/pressespiegel/2021-02-18-schwaebische.jpeg))

## 17.2.2021
* Schwäbische: [Klima-Aktivisten hängen Banner am Untertor auf](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-auf-_arid,11329632.html)
* Schwäbische: [Allianz wehrt sich gegen Kritik von Klimaschützern](https://www.schwaebische.de/landkreis/landkreis-sigmaringen/sigmaringen_artikel,-allianz-wehrt-sich-gegen-kritik-von-klimaschuetzern-_arid,11330158.html) ([Foto](/pressespiegel/2021-02-17-schwaebische-2.jpeg))
* Ravensburger Spectrum: [Jugend: Baum-Camp oder Bahnhof-Szene ..?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2622937/ravensburg-baum-camp-oder-bahnhof-szene-)
* BLIX: [FDP: Rebellion gegen den Regionalplan ist ein Bündnis für Stillstand](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/3873-fdp-rebellion-gegen-den-regionalplan-ist-ein-buendnis-fuer-stillstand)
* BLIX: [SPD fordert Kommunikation und Verständnis für Klimaneutralität](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/3874-spd-fordert-kommunikation-und-verstaendnis-fuer-klimaneutralitaet)

## 16.2.2021
* Schwäbische: [„Kein Wachstum um jeden Preis“ - Umweltschützer rebellieren gegen Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-kein-wachstum-um-jeden-preis-umweltschuetzer-rebellieren-gegen-regionalplan-_arid,11329117.html) ([Foto](/pressespiegel/2021-02-16-schwaebische.jpeg))
* BLIX: [Ravensburger Klimacamp startet mit weiteren Banner-Aktionen und hat bisher keine Rechnung erhalten](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/3858-ravensburger-klimacamp-startet-mit-weiteren-banner-aktionen-und-hat-bisher-keine-rechnung-erhalten)

## 15.2.2021
* SatireSenf: [CDU Kreis Sigmaringen: Lügt die SchwäZ oder lädt die CDU Presse nur selektiv ein?](https://satiresenf.de/ts22-21-cdu-kreis-sigmaringen-luegt-die-schwaez-oder-laedt-die-cdu-presse-nur-selektiv-ein/)

## 14.2.2021
* Evangelisches Gemeindeblatt für Württemberg: Druck aus luftigen Höhen ([Foto](/pressespiegel/2021-evangelisches-gemeindeblatt.pdf))
* alibi -- studentischer Kulturverein e.V.: [alibi-Nicht-Live-Stream](https://youtu.be/eF3Lib2EHjE?t=3268) (Zeitstempel 54:28)

## 11.2.2021
* SWR4 Friedrichshafen

## 10.2.2021
* Ravensburger Spectrum: [Wiederholte manipulative, unwahre Berichterstattung der "Schwäbischen" (hier konkret: "Baumbesetzung")](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2606345/wiederholte-manipulative-unwahre-berichterstattung-der-schwabischen-hier-ko)

## 9.2.2021
* Schwäbische: [Baumbesetzung: Stadt muss Polizeieinsatz bezahlen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-baumbesetzung-stadt-muss-polizeieinsatz-bezahlen-_arid,11326276.html)
* Schwäbische: [Stadt Ravensburg schickt Rechnung an Baumbesetzer](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-ravensburg-schickt-rechnung-an-baumbesetzer-_arid,11326797.html)
* Ravensburger Spectrum: [Räumung Baumcamp: Die Frage ist nicht "ob", sondern "wann": Rücktritt des Ravensburger Bürgermeisters und anderer Verantwortlicher gefordert](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2604031/raumung-baumcamp-die-frage-ist-nicht-ob-sondern-wann-rucktritt-des-ravensbu)

## 8.2.2021
* Schwäbische: [Klimaaktivisten demonstrieren friedlich gegen Regionalplan](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klimaaktivisten-demonstrieren-friedlich-gegen-regionalplan-_arid,11325800.html) ([Foto](/pressespiegel/2021-02-08-schwaebische.jpeg))

## 7.2.2021
* Ravensburger Spectrum: [Brisant: Ravensburger Baumbesetzer/innen- Thema im Landtag BW](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2598816/brisant-ravensburger-baumbesetzerinnen--thema-im-landtag-bw)

## 6.2.2021
* Schwäbische: [Karikatur von Rainer Weishaupt](/pressespiegel/2021-02-06-schwaebische.jpeg): In den künftigen Ravensburger Wohngebieten ist kein Platz für alle 500 Interessenten. Kein Grund zur Enttäuschung, neue Ideen braucht das Land. Die Etagenwohnung ist künftig der Schlüssel zum Glück: Wer frei von Höhenangst ist, könnte unter fachkundiger Anleitung seinen Traum vom Eigenheim so doch noch verwirklichen.
* SWR Aktuell: [Ravensburger Klimaaktivisten protestieren gegen Regionalplan und steigen Regionalverband aufs Dach](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/protest-gegen-regionalplan-bodensee-oberschwaben-102.html)
* Ravensburger Spectrum: [Junge Umweltaktivist/innen: Hausdach statt Baumkrone besetzt](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2596822/junge-umweltaktivistinnen-hausdach-statt-baumkrone-besetzt)
* Wochenblatt-online: [Klimaaktivist\*innen verhüllen Regionalverbandsgebäude](https://wochenblatt-online.de/klimaaktivistinnen-verhuellen-regionalverbandsgebaeude/)
* BLIX: [Klimaaktivist\*innen verhüllen Regionalverbandsgebäude, um auf von dort ausgehende Klimazerstörung hinzuweisen](https://www.diebildschirmzeitung.de/diebildschirmzeitung/diebildschirmzeitung-le/3745-klimaaktivist-innen-verhuellen-regionalverbandsgebaeude-um-auf-von-dort-ausgehende-klimazerstoerung-hinzuweisen)

## 2.2.2021
* Wochenblatt-online: [Baumbesetzer zur Rechen­schaft ziehen](https://wochenblatt-online.de/baumbesetzer-zur-rechenschaft-ziehen/)

## 1.2.2021
* Schwäbische: [Klima-Aktivisten bauen Baumcamp in Ravensburg ab](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klima-aktivisten-bauen-baumcamp-in-ravensburg-ab-_arid,11322887.html)

## 31.1.2021
* Ravensburger Spectrum: [Baumcamp weg - Ultimatum da](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2581836/baumcamp-weg---ultimatum-da)

## 30.1.2021
* Schwäbische: [Ravensburger Baumbesetzer ziehen Bilanz: „Wir erlebten großen Zuspruch“](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-ziehen-bilanz-wir-erlebten-grossen-zuspruch-_arid,11322187.html) ([Foto](/pressespiegel/2021-01-30-schwaebische.jpeg))

## 29.1.2021
* Ravensburger Spectrum: [Kundgebung der "Baumhausaktivist/innen"](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2577470/pressekonferenz-der-baumhausaktivistinnen-morgen -samstag-30-januar-2021-14-)

## 28.1.2021
* Regio TV Bodensee: [Nach einer Absprache mit der Stadt werden die Klimaaktivisten ihr Baumhaus am Rande der Ravensburger Innenstadt freiwillig räumen – der Protest soll an anderer Stelle weitergehen.](https://www.regio-tv.de/mediathek/video/regio-tv-bodensee-journal-28-01-2021/)

## 26.1.2021
* Ravensburger Spectrum: ["Klimaschutz" in Ravensburg ist nur lautes Getöse, geblasen aus Hörnern von Absichten](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2567796/)

## 23.1.2021
* Schwäbische: [Karikatur von Rainer Weishaupt](/pressespiegel/schwaebische-2021-01-23.jpeg)

## 22.1.2021
* Wochenblatt: [Klimacamper\*innen pausieren ihr Baumhaus und stellen Stadt Ultimatum](https://www.wochenblatt-news.de/klimacamperinnen-pausieren-ihr-baumhaus-und-stellen-stadt-ultimatum/)
* Schwäbische: [Ravensburger Baumbesetzer räumen ihr Lager](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-raeumen-ihr-lager-_arid,11319583.html)
* Schwäbische: [Stadtrat und Vater einer Baumbesetzerin fordert mehr Klimaschutz](https://www.schwaebische.de/landkreis/landkreis-ravensburg/bad-waldsee_artikel,-stadtrat-und-vater-einer-baumbesetzerin-fordert-mehr-klimaschutz-_arid,11318989.html)
* Ravensburger Spectrum: [Ravensburg Anno Domini 2034 -- so ... ? Oder so ... ?](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2557756/ravensburg-anno-domini-2034---so---oder-so--)
* Ravensburger Spectrum: [+++ Baumbesetzung +++ 5G +++ Corona +++ WEM GEHÖRT DIE STADT? - AUSZUG AUS EINER BACHELOR-ARBEIT (hoch spannend und aufschlussreich)](https://ravensburger-spectrum.mozello.de/deutschland-1/params/post/2558364/wem-gehort-die-stadt---auszug-aus-einer-bachelor-arbeit-hoch-spannend-und-a)

## 21.1.2021
* Schwäbische: [Keine Einigung zwischen Stadt und Baumbesetzern](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-keine-einigung-zwischen-stadt-und-baumbesetzern-_arid,11318553.html) ([Foto](/pressespiegel/2021-01-21-schwaebische.jpeg))
* Stuttgarter Zeitung: [Kein Friede dieser Hütte](https://www.stuttgarter-zeitung.de/inhalt.baumbesetzer-im-beschaulichen-ravensburg-kein-friede-dieser-huette.a2c532e1-60ba-493a-9782-a0d93c874f5a.html?reduced=true) ([Foto](/pressespiegel/2021-01-21-stuttgarter-zeitung.pdf))
* seemoz: [Ohne Bäume keine Träume](https://www.seemoz.de/lokal_regional/ohne-baeume-keine-traeume/)

## 20.1.2021
* Ravensburger Spectrum: [Baumhausdiskurs - Wem gehört eigentlich der öffentliche Raum? - Schlaglicht auf eine "Grauzone" ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2550419/baumhausdiskurs---wem-gehort-eigentlich-der-offentliche-raum---schlaglicht-)

## 19.1.2021
* Wir und jetzt (Blogeintrag): [Baumhausklimacamp in Ravensburg](https://wirundjetzt.org/baumhausklimacamp-in-ravensburg/)

## 15.1.2021
* Regio TV Bodensee: [Nachrichten vom 15.1.2020, Beginn bei 7:51](https://www.regio-tv.de/mediathek/video/regio-tv-bodensee-journal-15-01-2021/)
* Schwäbische: [Ravensburger Baumbesetzer erstmals im Gespräch mit der Stadt: Das sind die Streitpunkte](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-erstmals-im-gespraech-mit-der-stadt-das-sind-die-streitpunkte-_arid,11316775.html) ([Foto](/pressespiegel/2021-01-15-schwaebische.jpeg))
* Schwäbische: Baumbesetzer laufen Gefahr, Sympathie zu verspielen ([Foto](/pressespiegel/2021-01-15-schwaebische-2.jpeg))
* Süddeutsche: [Ravensburger Baumbesetzer dürfen erstmal bleiben](https://www.sueddeutsche.de/politik/demonstrationen-ravensburg-ravensburger-baumbesetzer-duerfen-erstmal-bleiben-dpa.urn-newsml-dpa-com-20090101-210115-99-41940)
* Wochenblatt-online: [Ravensburger Baumbesetzer dürfen erstmal bleiben](https://wochenblatt-online.de/ravensburger-baumbesetzer-duerfen-erstmal-bleiben/)

## 14.1.2021
* Schwäbische: [Zwischen Ravensburgs Oberbürgermeister und den Baumbesetzern kracht es](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-zwischen-ravensburgs-oberbuergermeister-und-den-baumbesetzern-kracht-es-_arid,11316370.html) ([Foto](/pressespiegel/2021-01-14-schwaebische.jpeg))
* Ravensburger Spectrum: [Umweltschutz: Der Ravensburger Weg ist unantastbar. (Über das Gepräch OB vs Samuel)](http://ravensburger-spectrum.mozello.de/ravensburg/params/post/2532846/umweltschutz-der-ravensburger-weg-ist-unantastbar-uber-das-geprach-ob-vs-sa)

## 13.1.2021
* KONTEXT:Wochenzeitung: [Ohne Bäume keine Träume](https://www.kontextwochenzeitung.de/schaubuehne/511/ohne-baeume-keine-traeume-7243.html)
* Ravensburger Spectrum: [Baumbesetzung (BB): Die Machtspiele des PIK-Buben (OB)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2526635/baumbesetzung-bb-die-machtspiele-des-pik-buben-ob)

## 11.1.2021
* Regio TV: [Klimacamp in Ravensburg: Aktivisten trotzen der Kälte](https://www.regio-tv.de/mediathek/video/klimacamp-in-ravensburg-aktivisten-trotzen-der-kaelte/)

## 10.1.2021
* Schwäbische: [Wipfel des Widerstandes: Zu Besuch bei den Ravensburger Klimaaktivisten](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-wipfel-des-widerstandes-zu-besuch-bei-den-ravensburger-klimaaktivisten-_arid,11314736.html) ([Foto](/pressespiegel/schwaebische-2021-01-10.jpeg))

## 8.1.2021
* Schwäbische: [Ravensburger Baumbesetzer fordern Zugeständnisse von der Stadtspitze](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-baumbesetzer-fordern-zugestaendnisse-von-der-stadtspitze-_arid,11314131.html) ([Foto](/pressespiegel/2021-01-08-schwaebische.pdf))
* Schwäbische: [Vier Fragen an die Ravensburger Baumbesetzer](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_video,-vier-fragen-an-die-ravensburger-baumbesetzer-_vidid,157865.html)

## 7.1.2021
* SWR Landesschau: [Klimaschützer bauen Baumhaus in Ravensburg](https://www.ardmediathek.de/ard/video/landesschau-baden-wuerttemberg/klimaschuetzer-bauen-baumhaus-in-ravensburg/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzEzODE4NjI/) (mit Anmoderation hier: [Mediathek](https://www.ardmediathek.de/swr/video/landesschau-baden-wuerttemberg/landesschau-baden-wuerttemberg-vom-7-1-2020/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzEzODE4OTM/))

## 5.1.2021
* Der Westallgäuer: Klimaaktivisten nisten sich in Baumhaus ein ([Foto](/pressespiegel/2021-01-05-westallgaeuer.pdf))
* BLIX: [„Wir brauchen die jungen Menschen in den Bäumen“](https://www.diebildschirmzeitung.de/blix/aktuell/3381-wir-brauchen-die-jungen-menschen-in-den-baeumen)

## 4.1.2021
* SWR: [Polizei duldet vorerst neues Klimaschutz-Baumhaus in Ravensburg](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/baumhaus-klimacamp-in-ravensburg-geraumt-100.html)

## 3.1.2021
* Schwäbische: [Polizeipräsident verteidigt Kompromiss mit Klima-Aktivisten nach erneuter Baumbesetzung](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-polizeipraesident-verteidigt-kompromiss-mit-klima-aktivisten-nach-erneuter-baumbesetzung-_arid,11312469.html) ([Foto](/pressespiegel/schwaebische-2021-01-03.jpeg))

## 2.1.2021
* Schwäbische: [Klima-Aktivisten besetzen erneut einen Baum in Ravensburg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klima-aktivisten-besetzen-erneut-einen-baum-in-ravensburg-_arid,11311887.html) ([Foto](/pressespiegel/schwaebische-2021-01-02.jpeg))

## 1.1.2021
* Bild: [Aktivisten besetzen Baum](https://www.bild.de/regional/stuttgart/stuttgart-aktuell/klimaschuetzer-aktivisten-besetzen-baum-74699230.bild.html)

## 31.12.
* Schwäbische: [Klima-Aktivisten besetzen erneut einen Baum in Ravensburg](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-klima-aktivisten-besetzen-erneut-einen-baum-in-ravensburg-_arid,11311753.html) ([Foto](/pressespiegel/2020-12-31-schwaebische.jpeg))
* Südkurier: [Polizei räumt Baumhaus-Klimacamp in Ravensburg. Nun wollen die Aktivisten klagen](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/polizei-raeumt-baumhaus-klimacamp-in-ravensburg-nun-wollen-die-aktivisten-klagen;art410936,10701777)
* Süddeutsche: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.sueddeutsche.de/politik/demonstrationen-ravensburg-klimaaktivisten-besetzen-schon-wieder-baum-in-ravensburg-dpa.urn-newsml-dpa-com-20090101-201231-99-862013)
* Badisches Tagblatt: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.badisches-tagblatt.de/Nachrichten/Klimaaktivisten-besetzen-schon-wieder-Baum-in-Ravensburg-69173.html)
* Zeit: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.zeit.de/news/2020-12/31/klimaaktivisten-besetzen-schon-wieder-baum-in-ravensburg)
* Stimme: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.stimme.de/suedwesten/nachrichten/pl/klimaaktivisten-besetzen-schon-wieder-baum-in-ravensburg;art19070,4433893)
* Landtag von Baden-Württemberg: [Klimaaktivisten besetzen schon wieder Baum in Ravensburg](https://www.landtag-bw.de/home/aktuelles/dpa-nachrichten/2020/Dezember/KW53/Donnerstag/23c434a3-6bd7-4b0e-b163-60583fbc.html)

## 30.12.
* Schwäbische: [SEK holt Klimaaktivisten nach 18 Tagen von Baum an Ravensburger Schussenstraße](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-sek-holt-klimaaktivisten-nach-18-tagen-von-baum-an-ravensburger-schussenstrasse-_arid,11311541.html) ([Foto](/pressespiegel/2020-12-30-schwaebische.jpeg))
* SWR: [Klimaschutz-Baumhaus in Ravensburg von Polizei geräumt](https://www.swr.de/swraktuell/baden-wuerttemberg/friedrichshafen/baumhaus-klimacamp-in-ravensburg-geraumt-100.html)
* SWR Aktuell (18:00 Uhr): [Klimaschutz-Baumhaus in Ravensburg von Polizei geräumt](https://www.ardmediathek.de/swr/video/swr-aktuell-baden-wuerttemberg/klimaschutz-baumhaus-in-ravensburg-von-polizei-geraeumt/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzEzNzc2NzQ/) (mit Anmoderation hier: [Mediathek](https://www.ardmediathek.de/swr/video/swr-aktuell-baden-wuerttemberg/sendung-18-00-uhr-vom-30-12-2020/swr-baden-wuerttemberg/Y3JpZDovL3N3ci5kZS9hZXgvbzEzNzc2Nzc/), Zeitstempel 4:13)
* Badische Zeitung: [Klimaaktivisten besetzen wochenlang Baum in Ravensburg](https://www.badische-zeitung.de/klimaaktivisten-besetzen-wochenlang-baum-in-ravensburg--199217771.html)
* Zeit: [Klimaaktivisten besetzen wochenlang Baum in Ravensburg](https://www.zeit.de/news/2020-12/30/klimaaktivisten-besetzen-wochenlang-baum-in-ravensburg)
* Wochenblatt: [Räumung des Ravensburger Klimacamps in Baumkrone](https://www.wochenblatt-news.de/raeumung-des-ravensburger-klimacamps-in-baumkrone/)
* Ravensburger Spectrum: [Pressekonferenz der Ravensburger Baumbesetzer (Teil 1 – Prolog)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2438292/pressekonferenz-der-ravensburger-baumbesetzer), [Teil 2 (mit Video)](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2438890/uber-die-demo-pressekonferenz-der-ravensburger-baumbestzer-teil-2-2)
* Die Bildschirmzeitung: [Stadt Ravensburg lässt Baumhaus polizeilich räumen](https://www.diebildschirmzeitung.de/diebildschirmzeitung/landkreis-ravensburg/3365-stadt-ravensburg-laesst-baumhaus-polizeilich-raeumen)
* Donau 3 FM: [Ravensburg: Polizei holt 17-jährigen Aktivisten von Baum](https://www.donau3fm.de/ravensburg-polizei-holt-17-jaehrigen-aktivisten-von-baum-157356/)
* Landtag von Baden-Württemberg: [Klimaaktivisten besetzen wochenlang Baum in Ravensburg](https://www.landtag-bw.de/home/aktuelles/dpa-nachrichten/2020/Dezember/KW53/Mittwoch/6f520321-a54f-4416-a1b6-3ad0b06a.html)
* Welt: [Klimaaktivisten besetzen wochenlang Baum in Ravensburg](https://www.welt.de/regionales/baden-wuerttemberg/article223483416/Klimaaktivisten-besetzen-wochenlang-Baum-in-Ravensburg.html)

## 29.12.
* Schwäbische: [Stadt lässt Ravensburger Klimacamp räumen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-laesst-ravensburger-klimacamp-raeumen-_arid,11311295.html)
* Ravensburger Spectrum: [Skandal oder Sensation? – Mitglied der Ravensburger "Klimakommission" wird selbst zum Baumbesetzer!](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2431340/)
* Ravensburger Spectrum: [Ravensburg Eilmeldung: Baumhaus wird geräumt!!! Für zwei Aktivisten rücken 14 Großraumwagen der Göppinger Polizei an ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2432583/ravensburg-eilmedlung-baumhaus-wird-geraumt)

## 28.12.
* Schwäbische: Kultur leben ([Foto](/pressespiegel/2020-12-28-schwaebische.jpeg))
* Ravensburger Spectrum: [Ravensburg: Ein Professor (61) besetzt statt Pult einen Baum – "Dozent für praktischen Umweltschutz"](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2425526/ravensburg-ein-professor-besetzt-statt-pult-einen-baum-solidaritat-uber-alt)

## 27.12.
* Ravensburger Spectrum: [Heute Morgen: Interview mit Ravensburger Baumbesetzer/Baumschützer](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2418722/interview-heute-morgen-mit-ravensburger-baumbesetzerbaumschutzer)

## 25.12.
* Ravensburger Spectrum: [Tree alive – Offener Brief an die Ravensburger Baumschützer ...](https://ravensburger-spectrum.mozello.de/ravensburg/params/post/2408268/tree-alive---offener-brief-an-die-ravensburger-baumschutzer-)

## 24.12.
* Schwäbische: [Ravensburger Klima-Aktivisten wollen auch über Weihnachten im Baum bleiben](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-klima-aktivisten-wollen-auch-ueber-weihnachten-im-baum-bleiben-_arid,11309774.html) ([Foto](/pressespiegel/2020-12-24-schwaebische.jpeg))

## 20.12.
* Schwäbische: [Stadt Ravensburg will Aktivisten zum Aufgeben bewegen](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-stadt-ravensburg-will-aktivisten-zum-aufgeben-bewegen-_arid,11308373.html) ([Foto](/pressespiegel/2020-12-20-schwaebische.jpeg))

## 18.12.
* Radio 7

## 16.12.
- Südkurier: [Mit einer Baumbesetzung in Ravensburg protestieren Jugendliche gegen die Zerstörung von Wäldern für den Straßenbau](https://www.suedkurier.de/region/bodenseekreis/bodenseekreis/mit-einer-baumbesetzung-in-ravensburg-protestieren-jugendliche-gegen-die-zerstoerung-von-waeldern-fuer-den-strassenbau;art410936,10691560)

## 14.12.
* Wochenblatt: [Junge Aktivisten errichten Klimacamp in Baumkrone](https://www.wochenblatt-news.de/junge-aktivisten-errichten-klimacamp-in-baumkrone/)

## 13.12.

* Schwäbische: [Ravensburger Klima-Aktivisten fordern Einhaltung des Pariser Klimaabkommens](https://www.schwaebische.de/landkreis/landkreis-ravensburg/ravensburg_artikel,-ravensburger-klima-aktivisten-fordern-einhaltung-des-pariser-klimaabkommens-_arid,11305454.html)
* Radio 7
