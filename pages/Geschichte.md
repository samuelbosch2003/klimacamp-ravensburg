---
layout: page
title: Unsere Geschichte
permalink: /geschichte/
nav_order: 40
---


# Unsere Geschichte

## Gründung

**Das Klimacamp Ravensburg startete am 12.12.2020 in einer Linde an der Schussenstraße als Besetzung, um die Politik in Ravensburg auf ihren Handlungsbedarf aufmerksam zu machen.**

Anfangs bestand es aus einer einfachen Plattform namens *Oi 2.0* welche aus einigen Balken und zwei Paletten bestand.

## Idee eines dauerhaften Klimacamps

Es entwickelte sich in den nächsten Wochen zu einem Klimacamp nach den Beispielen in Augsburg, Hamburg, Nürnberg und anderen Städten. Es brachte vielen Menschen tolle Inspirationen für Aktionen.
Immer mehr Menschen solidarisierten sich mit der Baumbesetzung oder wurden selbst aktiv.

Nach einer Woche wurde die Plattform zu einem Baumhaus vergrößert und das Barrio namens *Immer* ausgerufen.
Die lokalen Zeitungen wurden jetzt zunehmend aufmerksamer auf das Baumhaus.
Der Zuspruch aus der Bevölkerung war großartig. Viele Bürger:innen kamen, um die Aktion zum Beispiel mit warmem Tee zu unterstützen, und schrieben fleißig Leser:innenbriefe.
<figure style="float: right">
  <a href="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/">
    <img src="/welcome.small.jpeg" alt="Das Baumhaus am 13.12.2020" width="384" height="226" style="width: 30em; height: auto; aspect-ratio: attr(width) / attr(height)">
      <img src="/warming-stripes.png" style="height: 0.5em; width: 30em; display: block">
  </a>
  <figcaption>Das Baumhaus am 13.12.2020</figcaption>
</figure>

## Weihnachtskundgebung

Am 24.12.2020 veranstalteten Unterstützer:innen eine
Weihnachtskonzertkundgebung im Park unter dem Baumhaus.
Es kamen etwa 30 Menschen und die Stimmung war gut.

## Traverse

Da die Stadt nach über zwei Wochen Besetzung immer noch nicht inhaltlich auf die Forderungen eingehen wollte und nicht einmal inhaltliche Gespräche mit der Baumbesetzung führen wollte, mussten die Klimagerechtigkeitsaktivist:innen den Druck auf die Politik erhöhen.

Hierzu spannten sie in der Nacht vom 26. auf den 27.12.2020 eine 40m lange Traverse (waagrechtes Polypropylen-Seil) in 12m Höhe über die Obere Breite Straße. Dort oben, oberhalb der Höhe der Ampel und weit über dem Autoverkehr, hängten die Aktivist:innen nun ein riesiges Banner (5m x 7m) mit der Aufschrift "1,5 Grad" auf.

Mit der Aktion sollte die Stadt nochmals stärker auf ihr dringend notwendiges Handeln hingewiesen werden.
Die Stadt Ravensburg hat mit der Klimakonsenskommission zwar einen Beschluss gefasst, welcher eine CO₂-Reduktion von 13 % pro Jahr vorschreibt. Wenn dieser Reduktionsprozess eingehalten werden würde, wäre Ravensburg bis 2040 CO₂-neutral.

**Leider reichen die beschlossenen Maßnahmen aber bei weitem nicht aus, um dieses Ziel zu erreichen.** 

Dies erklärt auch Prof. Dr. Ertel von der Hochschule Ravensburg-Weingarten immer wieder.

Es ist nicht einmal sicher, ob Ravensburg die bisher gesetzten Maßnahmen umsetzen wird.

## Die Räumung

**Die Stadt Ravensburg veranlasst am Abend des 29.12.2020 die Räumung des Baumhausklimacamps.**

Die Stadt wollte anscheinend nicht auf ihren Handlungsbedarf hingewiesen werden. 
Sie wollte mit der Räumung lieber den legitimen Protest der Aktivist:innen unterbinden um weiterhin nichts tun zu müssen!

Für die Räumung wurde ein großes Aufgebot an Polizei, SEK, Feuerwehr und Fahrzeugen wie Hebebühnen aufgefahren.

Im Baumhaus befand sich zum Zeitpunkt der Räumung eine minderjährige Person. Im Gegenzug fuhren die Einsatzkräfte mit ca. 60 Polizist:innen, ca. 10 SEKler:innen und mehreren Fahrzeugen der Feuerwehr auf.

Die Räumung verlief reibungslos.

Nach den ersten Minuten der Räumung entstand eine Solidemo aus ca. 60 Bürger:innen, welche auf der gegenüberliegenden Straßenseite mit ausreichend Abstand und Maske stattfand.

## Die Pressekonferenz

Am 30.12.2020 gab die Baumbesetzung eine Pressekonferenz am Baum, auf der Verteter:innen von verschiedenen Umweltschutz- und Klimagruppierungen aus der Region sprechen konnten. Im Anschluss konnten Fragen an die Vertreter:innen gestellt werden.

Durch die unvorhergesehene Räumung des Baumhauses erfuhr die Pressekonferenz einen noch größeren Andrang der Öffentlichkeit als es sonst vielleicht der Fall gewesen wäre.

Zur Pressekonferenz kamen zahlreiche Journalist:innen verschiedener Lokalzeitungen und Radiosender, aber auch zwei Fernsehsender.

Außerdem ca. 150 Unterstützer:innen, welche mit Abstand und Maske ebenfalls an der Pressekonferenz teilnahmen.

## Das neue Baumhaus

Ganz nach dem Motto **"Ihr könnt unsere Häuser zerstören, aber nicht die Kraft die sie schufen"** bauten die Aktivist:innen in der Nacht nach der Räumung ein neues Baumhaus (genannt *Danni*) in der Grünanlage an der Karlstarße.

**Durch das neue Baumhaus soll die Stadt weiter an den dringenden Handlungsbedarf zur Beschränkung der Erderhitzung auf 1,5° erinnert werden! Zudem muss sich die Stadt für einen zukunftsfähigen Regionalplan einsetzen.**

Nachdem das neue Baumhaus gebaut war, kam es nach einigem Hin und Her zu einem Gespräch zwischen den Besetzer:innen und der Polizei. 

Sie handelten gemeinsam einen "Friedensvertrag" aus, durch den es bis zum 10.1.2021 zu keiner Räumung und keinen neuen Baumhäusern kommt.

Die Aktivist:innen nutzen diese Zeit, um sich in Ruhe zu vernetzen und sich durch Kletterworkshops auf weitere Besetzungen vorzubereiten...
<figure style="float: right">
  <a href="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/#68">
    <img src="/neues-baumhaus.small.jpeg" alt="Unser neues Baumhaus (Foto vom 31.12.2020)" width="320" height="427" style="width: 30em; height: auto; aspect-ratio: attr(width) / attr(height)">
  </a>
  <figcaption>Unser neues Baumhaus (31.12.2020)</figcaption>
</figure>



## Skillsharing

Am 9.1.2021 fand unser Kletter- und Baumhausbau-Skillsharing statt. Wir freuen
uns sehr, dass so viele interessierte Menschis da waren und das Skillsharing
ein echter Erfolg wurde! Es wurde sehr positiv aufgenommen und hat auch uns
total viel Spaß gemacht.

Es war perfektes Wetter (sonnig und teilweise leichter Schnee) und auch der
Umgang miteinander war respektvoll und total entspannt. Zwischen 25 und 30
Menschis waren da und haben zunächst die Kletter-Basics gelernt und diese dann
selbst anderen beigebracht, sodass kleine Grüppchen, vorwiegend an Nadelbäumen,
sich sichern, klettern und abseilen lernten und dann in einem zweiten Schritt
das baumschonende Anbinden von Rundhölzern an Bäumen. Das Ziel war es, Menschis
das Klettern und Bauen von Baumhäusern beizubringen und die Protestform der
Baumbesetzung näher zu bringen.

Alle Teilnehmer\*innen trugen dauerhaft eine Mund-Nasen-Bedeckung auf und
hielten den Mindestabstand zu anderen ein. Zusammen mit der Tatsache, dass das
Skillsharing an der frischen Luft stattfand, sollte damit eine Infektion so gut
wie ausgeschlossen sein. Trotzdem bitten wir euch, uns zu informieren, wenn ihr
im Nachgang vermutet oder bestätigt bekommt, dass ihr Corona habt. Dann könnten
wir die anderen Teilnehmer\*innen informieren.

Das Skillsharing fand an einem geeigneten Ort statt. Die Teilnehmer\*innen
konnten sich vorher an einem zuvor abgemachten Treffpunkt treffen, um dann zum
Zielort geshuttlet zu werden. Wir haben uns gezielt dazu entschlossen, den
Fokus auf die Lehrveranstaltung als solche und nicht diese als Protestaktion zu
legen. Nachdem der Polizeipräsident erst wollte, dass wir unser zweites
Baumhaus innerhalb des Parks umziehen und dies auch eine super Möglichkeit für
das Skillsharing gewesen wäre, änderte er später seine Meinung und möchte nun
vorerst keine Veränderung. Ein Baumhausbau in den Parkanlagen des Stadtgebiets
hätte also einen Polizeieinsatz verursacht — keine guten Lernbedingungen —
daher lieber der andere geeignete Ort. Von Presse sowie Polizei wurden wir aber
nicht besucht, wodurch wir ungestört lernen und Wissen weitergeben konnten 😊. 

Nochmal vielen lieben Dank an alle Interessierten die da waren und das
Skillsharing zu einem so tollen Erlebnis machten! 🌱🌳🌲



<figure>
  <a href="https://www.speicherleck.de/iblech/stuff/ravensburg-klimacamp/#54">
    <img src="/ertel.small.jpeg" alt="Prof. Dr. Wolfgang Ertel im Baumhaus" width="640" height="360" style="display: block; width: 100%; height: auto; aspect-ratio: attr(width) / attr(height)">
  </a>
  <figcaption>Prof. Dr. Ertel im Baumhaus am 29.12.2020</figcaption>
</figure>

